# Len Aktiva Tetap BackEnd #

Source code aplikasi pengendalian aktiva tetap PT. Len Industri (Persero) untk Back End.

### Requirement ###

* Visual Studio Community 2017
* MySql.Data.EntityFrameworkCore (prerelease)
* .NET Core 1.1

### Parts ###

* ApatDataAccess, console library untuk Database Context
* ApatWebApi, aplikasi REST 

### Contact ###

* Jeki Maulana -> jekibus[at]gmail.com