﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

/// <summary>
/// Data Log untuk aplikasi Apat
/// 1. Error Log dalam bentuk file
/// 2. Audit Trail (perubahan data)
/// 3. Log Trail (aktivitas user)
/// </summary>
namespace ApatLog
{
    /// <summary>
    /// 1. Error Log dalam bentuk file
    /// </summary>
    public static partial class LogFile
    {
        /// <summary>
        /// Level dampak yang tersedia.
        /// </summary>
        public enum ImpactLevel
        {
            High = 0,
            Medium = 1,
            Low = 2,
        }

        /// <summary>
        /// Penyimpanan log untuk low priority.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public static void SaveToFile(this Exception ex)
        {
            SaveToFile(ex, ImpactLevel.Low, ex.ToString());
        }

        /// <summary>
        /// Penyimpanan log untuk spesifik priority.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <param name="impactLevel">The Impact level.</param>
        public static void SaveToFile(this Exception ex, ImpactLevel impactLevel)
        {
            SaveToFile(ex, impactLevel, ex.ToString());
        }

        /// <summary>
        /// Penyimpanan log untuk spesifik priority dan deskripsi.
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <param name="impactLevel">Level dampak yang terjadi.</param>
        /// <param name="errorDescription">Deskripsi error.</param>
        public static void SaveToFile(this Exception ex, ImpactLevel impactLevel, string errorDescription)
        {
            try
            {
                LogFileModel log = new LogFileModel();
                if (errorDescription != null && errorDescription != "")
                {
                    log.ErrorShortDescription = errorDescription;
                }
                log.ExceptionType = ex.GetType().FullName;
                StackTrace stackTrace = new StackTrace(ex, true);
                var allFrames = stackTrace.GetFrames().ToList();

                foreach (var frame in allFrames)
                {
                    log.FileName = frame.GetFileName();
                    log.LineNumber = frame.GetFileLineNumber();
                    var method = frame.GetMethod();
                    log.MethodName = method.Name;
                    log.ClassName = frame.GetMethod().DeclaringType.ToString();
                }

                log.ImpactLevel = impactLevel.ToString();

                //TODO get application name
                log.ApplicationName = "Apat";

                // TODO dev mode log
                log.ErrorMessage = ex.Message + " " + ex.InnerException + ex.ToString();
                log.StackTrace = ex.StackTrace;

                //if (ex.InnerException != null)
                //{
                //    log.InnerException = ex.InnerException.ToString();
                //    log.InnerExceptionMessage = ex.InnerException.Message;
                //}

                //TODO get the ip address
                log.IpAddress = "";

                DateTime dateTime = DateTime.Now;
                log.Time = dateTime.ToString();

                //TODO get username user login
                log.Username = "Public";

                string print = log.Time + "\t" + log.ClassName + "." + log.MethodName + "\tLine: " + log.LineNumber + "\tMessage: " + log.ErrorMessage + "\tUser: " + log.Username + " " + log.ErrorShortDescription + "\tLevel: " + log.ImpactLevel;

                //TODO print to another thing
                Console.WriteLine(print);

                string timeFile = dateTime.Year.ToString() + dateTime.Month.ToString("00") + dateTime.Day.ToString("00");

                string rootDirectory = @"c:\Apat\Log\";
                string fileName = "ApatLog - ";

                if (!Directory.Exists(rootDirectory))
                {
                    Directory.CreateDirectory(rootDirectory);
                }

                using (StreamWriter sw = File.AppendText(rootDirectory + fileName + timeFile + ".log"))
                {
                    sw.WriteLine(print);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
