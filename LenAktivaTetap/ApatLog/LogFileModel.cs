﻿/// <summary>
/// Data Log untuk aplikasi Apat
/// 1. Error Log dalam bentuk file
/// 2. Audit Trail (perubahan data)
/// 3. Log Trail (aktivitas user)
/// </summary>
namespace ApatLog
{
    /// <summary>
    /// Model untuk print log file
    /// </summary>
    class LogFileModel
    {
        /// <summary>
        /// Deskripsi error
        /// </summary>
        public string ErrorShortDescription { get; set; }

        /// <summary>
        /// Tipe error
        /// </summary>
        public string ExceptionType { get; set; }

        /// <summary>
        /// Nama file yang error
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Baris yang error
        /// </summary>
        public int LineNumber { get; set; }

        /// <summary>
        /// Nama method yang error
        /// </summary>
        public string MethodName { get; set; }

        /// <summary>
        /// Nama class yang error
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Level error
        /// </summary>
        public string ImpactLevel { get; set; }

        /// <summary>
        /// Nama Aplikasi
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Konten error
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Model StackTrace yang akan dijadikan acuan
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Ip Address pengguna yang mendapatkan error
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Waktu terjadinya error
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Username pengguna yang mendapatkan error
        /// </summary>
        public string Username { get; set; }

        //public string InnerException { get; set; }
        //public string InnerExceptionMessage { get; set; }
    }
}
