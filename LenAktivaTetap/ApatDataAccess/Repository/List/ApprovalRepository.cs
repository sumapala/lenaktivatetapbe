﻿using ApatDataAccess.Models.List;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ApatLog;
using System;
using ApatDataAccess.Models;

/// <summary>
/// Repository untuk List Approval Barang
/// </summary>
namespace ApatDataAccess.Repository.List
{
    /// <summary>
    /// Class untuk repository List Approval Barang
    /// Berdasarkan interface IRepository
    /// Menggunakan model ListApproval Barang
    /// </summary>
    public class ApprovalRepository : IRepository<ListApproval>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Memasukan data dalam bentuk item ke database ListApproval
        /// </summary>
        /// <param name="item">Item Jenis yang akan dimasukan berdasarkan model ListApproval</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public ListApproval Insert(ListApproval item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database ListApproval
        /// </summary>
        /// <param name="item">Item Jenis yang akan diubah berdasarkan model ListApproval</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public ListApproval Update(ListApproval item)
        {
            try
            {
                ListApproval data = db.ListApproval.FirstOrDefault(d => d.ListApprovalId == item.ListApprovalId);
                if(data != null)
                {
                    data.ApprovalStatusApproverId1 = item.ApprovalStatusApproverId1;
                    data.ApprovalStatusApproverId2 = item.ApprovalStatusApproverId2;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database ListApproval
        /// </summary>
        /// <param name="id">ID Item Jenis barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public ListApproval Delete(int id)
        {
            try
            {
                ListApproval item = db.ListApproval.Find(id);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new ListApproval();
            }
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database ListApproval
        /// </summary>
        /// <returns>List item Jenis barang</returns>
        public IList<ListApproval> GetAll()
        {
            try
            {
                DbSet<ListApproval> result = db.ListApproval;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<ListApproval>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database ListApproval
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item Jenis barang dalam bentuk angka</param>
        /// <returns>List item Jenis barang</returns>
        public IList<ListApproval> GetById(int id)
        {
            try
            {
                DbSet<ListApproval> result = db.ListApproval;
                IQueryable<ListApproval> data = result.Where(d => d.ListApprovalId == id);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<ListApproval>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<ListApproval>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database ListApproval
        /// Menggunakan User ID tertentu
        /// </summary>
        /// <param name="userId">ID User dalam bentuk angka</param>
        /// <returns>List item Approval barang</returns>
        public IList<ListApproval> GetByUserId(int userId)
        {
            try
            {
                DbSet<ListApproval> result = db.ListApproval;
                IQueryable<ListApproval> data = result.Where(d => d.SubmitterId == userId);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<ListApproval>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<ListApproval>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List AT pada database DII
        /// Menggunakan Approval ID tertentu
        /// </summary>
        /// <param name="listApprovalId">ID Approval dalam bentuk angka</param>
        /// <returns>List item AT</returns>
        public IList<Object> GetATByApprovalId(int listApprovalId)
        {
            try
            {
                ListApproval result = db.ListApproval.FirstOrDefault(d => d.ListApprovalId == listApprovalId);

                if (result != null)
                {
                    List<Object> data = new List<Object>();
                    switch (result.MenuId)
                    {
                        case 1:
                            DiatDetail dataItem = db.DiatDetail.FirstOrDefault(d => d.DiatId == result.DocumentId);
                            data.Add(dataItem);
                            break;
                        default:
                            break;
                    }

                    if(data.Count() > 0)
                    {
                        return data;
                    }
                }
                return new List<Object>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Object>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database ListApproval
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item Jenis barang dalam bentuk string</param>
        /// <returns>List item Jenis barang</returns>
        public IList<ListApproval> GetByNo(string no)
        {
            try
            {
                DbSet<ListApproval> result = db.ListApproval;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<ListApproval>();
            }
        }
    }
}
