﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;

namespace ApatDataAccess.Repository
{
    public class MerkRepository : IRepository<MasterMerk>
    {
        private ApatContext db = ApatContext.Connect();

        public MasterMerk Insert(MasterMerk item)
        {
            db.Add(item);
            db.SaveChanges();
            return item;
        }

        public MasterMerk Update(MasterMerk item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public MasterMerk Delete(int id)
        {
            var item = db.MasterMerk.Find(id);
            db.SaveChanges();
            return item;
        }

        public IList<MasterMerk> GetAll()
        {
            var res = db.MasterMerk;
            return res.ToList();
        }

        public IList<MasterMerk> GetById(int id)
        {
            var res = db.MasterMerk;
            return res.Where(d => d.MerkId == id).ToList();
        }

        public IList<MasterMerk> GetByNo(string no)
        {
            var res = db.MasterMerk;
            return res.ToList();
        }
    }
}
