﻿using ApatDataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ApatLog;
using System;

/// <summary>
/// Repository untuk Master Jenis Barang
/// </summary>
namespace ApatDataAccess.Repository.Master
{
    /// <summary>
    /// Class untuk repository Master Jenis Barang
    /// Berdasarkan interface IRepository
    /// Menggunakan model MasterJenis Barang
    /// </summary>
    public class MappingAktivaRepository : IRepository<MappingAktiva>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Memasukan data dalam bentuk item ke database MappingAktiva
        /// </summary>
        /// <param name="item">Item Jenis yang akan dimasukan berdasarkan model MappingAktiva</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MappingAktiva Insert(MappingAktiva item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database MappingAktiva
        /// </summary>
        /// <param name="item">Item Jenis yang akan diubah berdasarkan model MappingAktiva</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MappingAktiva Update(MappingAktiva item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database MasterJenis
        /// </summary>
        /// <param name="id">ID Item Jenis barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public MappingAktiva Delete(int id)
        {
            try
            {
                MappingAktiva item = db.MappingAktiva.Find(id);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new MappingAktiva();
            }
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database MasterJenis
        /// </summary>
        /// <returns>List item Jenis barang</returns>
        public IList<MappingAktiva> GetAll()
        {
            try
            {
                DbSet<MappingAktiva> result = db.MappingAktiva;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MappingAktiva>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterJenis
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item Jenis barang dalam bentuk angka</param>
        /// <returns>List item Jenis barang</returns>
        public IList<MappingAktiva> GetById(int id)
        {
            try
            {
                DbSet<MappingAktiva> result = db.MappingAktiva;
                IQueryable<MappingAktiva> data = result.Where(d => d.MappingId == id);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MappingAktiva>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MappingAktiva>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterJenis
        /// Menggunakan Jenis ID tertentu
        /// </summary>
        /// <param name="subKelompokId">ID Jenis barang dalam bentuk angka</param>
        /// <returns>List item Jenis barang</returns>
        public IList<MappingAktiva> GetBySubKelompokId(int subKelompokId)
        {
            try
            {
                DbSet<MappingAktiva> result = db.MappingAktiva;
                IQueryable<MappingAktiva> data = result.Where(d => d.MappingId == subKelompokId);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MappingAktiva>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MappingAktiva>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterJenis
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item Jenis barang dalam bentuk string</param>
        /// <returns>List item Jenis barang</returns>
        public IList<MappingAktiva> GetByNo(string no)
        {
            try
            {
                DbSet<MappingAktiva> result = db.MappingAktiva;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MappingAktiva>();
            }
        }
    }
}
