﻿using ApatDataAccess.Models.Master;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ApatLog;
using System;

/// <summary>
/// Repository untuk Master Kelompok Barang
/// </summary>
namespace ApatDataAccess.Repository.Master
{
    /// <summary>
    /// Class untuk repository Master Kelompok Barang
    /// Berdasarkan interface IRepository
    /// Menggunakan model MasterKelompok Barang
    /// </summary>
    public class KelompokRepository : IRepository<MasterKelompok>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Memasukan data dalam bentuk item ke database MasterKelompok
        /// </summary>
        /// <param name="item">Item kelompok yang akan dimasukan berdasarkan model MasterKelompok</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterKelompok Insert(MasterKelompok item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database MasterKelompok
        /// </summary>
        /// <param name="item">Item kelompok yang akan diubah berdasarkan model MasterKelompok</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterKelompok Update(MasterKelompok item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database MasterKelompok
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public MasterKelompok Delete(int id)
        {
            try
            {
                MasterKelompok item = db.MasterKelompok.Find(id);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new MasterKelompok();
            }
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database MasterKelompok
        /// </summary>
        /// <returns>List item kelompok barang</returns>
        public IList<MasterKelompok> GetAll()
        {
            try
            {
                DbSet<MasterKelompok> result = db.MasterKelompok;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterKelompok>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterKelompok
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>List item kelompok barang</returns>
        public IList<MasterKelompok> GetById(int id)
        {
            DbSet<MasterKelompok> result = db.MasterKelompok;
            IQueryable<MasterKelompok> data = result.Where(d => d.MasterKelompokId == id);
            if (data != null)
            {
                return data.ToList();
            }
            else
            {
                return new List<MasterKelompok>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterKelompok
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item kelompok barang dalam bentuk string</param>
        /// <returns>List item kelompok barang</returns>
        public IList<MasterKelompok> GetByNo(string no)
        {
            try
            {
                DbSet<MasterKelompok> result = db.MasterKelompok;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterKelompok>();
            }
        }
    }
}
