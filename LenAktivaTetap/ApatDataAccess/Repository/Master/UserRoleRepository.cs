﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;
using ApatLog;

/// <summary>
/// Namespace Repository
/// </summary>
namespace ApatDataAccess.Repository
{
    /// <summary>
    /// Class User Role Repository
    /// </summary>
    public class UserRoleRepository : IRepository<UserRole>
    {

        /// <summary>
        /// Inisiasi database context
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Simpan data User Role
        /// </summary>
        /// <param name="item"></param>
        /// <returns>model UserRole</returns>
        public UserRole Insert(UserRole item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch(Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Ubah data User Role
        /// </summary>
        /// <param name="item"></param>
        /// <returns>model User Role</returns>
        public UserRole Update(UserRole item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }

        }

        /// <summary>
        /// Hapus data User Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Model UserRole</returns>
        public UserRole Delete(int id)
        {
            try
            {
                var item = db.UserRole.Find(id);
                db.SaveChanges();
                return item;
            }
            catch(Exception e)
            {
                e.SaveToFile();
                return new UserRole();
            }
            
        }

        /// <summary>
        /// Baca seluruh data User Role
        /// </summary>
        /// <returns>List User Role</returns>
        public IList<UserRole> GetAll()
        {
            try
            {
                var res = db.UserRole;
                return res.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<UserRole>();
            }

        }

        /// <summary>
        /// Baca data User Role berdasarkan paramater Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List User Role</returns>
        public IList<object> GetById(int id)
        {
            IList<object> response = new List<object>();
            try
            {
                var res = from a in db.UserRole
                          join b in db.Role on a.RoleId equals b.RoleId
                          select new
                          {
                              a.UserRoleId,
                              a.UserId,
                              a.RoleId,
                              a.UnitKerjaId,
                              b.RoleName
                          };
                response.Add(res.Where(d => d.UserRoleId == id));
                //response.Add(res);
                return response;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                var res = from a in db.UserRole select a;
                response.Add(res);
                return response;
            }

        }

        /// <summary>
        /// [Tidak digunakan]
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        public IList<UserRole> GetByNo(string no)
        {
            var res = db.UserRole;
            return res.ToList();
        }
    }
}
