﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;

namespace ApatDataAccess.Repository
{
    public class RoleAppRepository : IRepository<RoleApp>
    {
        private ApatContext db = ApatContext.Connect();

        public RoleApp Insert(RoleApp item)
        {
            db.Add(item);
            db.SaveChanges();
            return item;
        }

        public RoleApp Update(RoleApp item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public RoleApp Delete(int id)
        {
            var item = db.Role.Find(id);
            db.SaveChanges();
            return item;
        }

        public IList<RoleApp> GetAll()
        {
            var res = db.Role;
            return res.ToList();
        }

        public IList<RoleApp> GetById(int id)
        {
            var res = db.Role;
            return res.Where(d => d.RoleId == id).ToList();
        }

        public IList<RoleApp> GetByNo(string no)
        {
            var res = db.Role;
            return res.ToList();
        }
    }
}
