﻿using ApatDataAccess.Models.Master;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ApatLog;
using System;

/// <summary>
/// Repository untuk Master Tipe Barang
/// </summary>
namespace ApatDataAccess.Repository.Master
{
    /// <summary>
    /// Class untuk repository Master Tipe Barang
    /// Berdasarkan interface IRepository
    /// Menggunakan model MasterTipe Barang
    /// </summary>
    public class TipeRepository : IRepository<MasterTipe>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Memasukan data dalam bentuk item ke database MasterTipe
        /// </summary>
        /// <param name="item">Item Tipe yang akan dimasukan berdasarkan model MasterTipe</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterTipe Insert(MasterTipe item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database MasterTipe
        /// </summary>
        /// <param name="item">Item Tipe yang akan diubah berdasarkan model MasterTipe</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterTipe Update(MasterTipe item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database MasterTipe
        /// </summary>
        /// <param name="id">ID Item Tipe barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public MasterTipe Delete(int id)
        {
            try
            {
                MasterTipe item = db.MasterTipe.Find(id);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new MasterTipe();
            }
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database MasterTipe
        /// </summary>
        /// <returns>List item Tipe barang</returns>
        public IList<MasterTipe> GetAll()
        {
            try
            {
                DbSet<MasterTipe> result = db.MasterTipe;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterTipe>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterTipe
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item Tipe barang dalam bentuk angka</param>
        /// <returns>List item Tipe barang</returns>
        public IList<MasterTipe> GetById(int id)
        {
            try
            {
                DbSet<MasterTipe> result = db.MasterTipe;
                IQueryable<MasterTipe> data = result.Where(d => d.MasterTipeId == id);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MasterTipe>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterTipe>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterTipe
        /// Menggunakan Tipe ID tertentu
        /// </summary>
        /// <param name="masterJenisId">ID Tipe barang dalam bentuk angka</param>
        /// <returns>List item Tipe barang</returns>
        public IList<MasterTipe> GetByJenisId(int jenisId)
        {
            try
            {
                DbSet<MasterTipe> result = db.MasterTipe;
                IQueryable<MasterTipe> data = result.Where(d => d.MasterJenisId == jenisId);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MasterTipe>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterTipe>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterTipe
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item Tipe barang dalam bentuk string</param>
        /// <returns>List item Tipe barang</returns>
        public IList<MasterTipe> GetByNo(string no)
        {
            try
            {
                DbSet<MasterTipe> result = db.MasterTipe;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterTipe>();
            }
        }
    }
}
