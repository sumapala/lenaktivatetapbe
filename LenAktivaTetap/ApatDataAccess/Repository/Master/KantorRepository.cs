﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;

namespace ApatDataAccess.Repository
{
    public class KantorRepository : IRepository<LokasiKantor>
    {
        private ApatContext db = ApatContext.Connect();

        public LokasiKantor Insert(LokasiKantor item)
        {
            db.Add(item);
            db.SaveChanges();
            return item;
        }

        public LokasiKantor Update(LokasiKantor item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public LokasiKantor Delete(int id)
        {
            var item = db.LokasiKantor.Find(id);
            db.SaveChanges();
            return item;
        }

        public IList<LokasiKantor> GetAll()
        {
            var res = db.LokasiKantor;
            return res.ToList();
        }

        public IList<LokasiKantor> GetById(int id)
        {
            var res = db.LokasiKantor;
            return res.Where(d => d.LokasiKantorId == id).ToList();
        }

        public IList<LokasiKantor> GetByNo(string no)
        {
            var res = db.LokasiKantor;
            return res.ToList();
        }
    }
}
