﻿using ApatDataAccess.Models.Master;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ApatLog;
using System;

/// <summary>
/// Repository untuk Master Sub Kelompok Barang
/// </summary>
namespace ApatDataAccess.Repository.Master
{
    /// <summary>
    /// Class untuk repository Master Sub Kelompok Barang
    /// Berdasarkan interface IRepository
    /// Menggunakan model MasterSubKelompok Barang
    /// </summary>
    public class SubKelompokRepository : IRepository<MasterSubKelompok>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Memasukan data dalam bentuk item ke database MasterSubKelompok
        /// </summary>
        /// <param name="item">Item Sub kelompok yang akan dimasukan berdasarkan model MasterSubKelompok</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterSubKelompok Insert(MasterSubKelompok item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database MasterSubKelompok
        /// </summary>
        /// <param name="item">Item Sub kelompok yang akan diubah berdasarkan model MasterSubKelompok</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterSubKelompok Update(MasterSubKelompok item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database MasterSubKelompok
        /// </summary>
        /// <param name="id">ID Item Sub kelompok barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public MasterSubKelompok Delete(int id)
        {
            try
            {
                MasterSubKelompok item = db.MasterSubKelompok.Find(id);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new MasterSubKelompok();
            }
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database MasterSubKelompok
        /// </summary>
        /// <returns>List item Subkelompok barang</returns>
        public IList<MasterSubKelompok> GetAll()
        {
            try
            {
                DbSet<MasterSubKelompok> result = db.MasterSubKelompok;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterSubKelompok>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterSubKelompok
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item Sub kelompok barang dalam bentuk angka</param>
        /// <returns>List item Sub kelompok barang</returns>
        public IList<MasterSubKelompok> GetById(int id)
        {
            try
            {
                DbSet<MasterSubKelompok> result = db.MasterSubKelompok;
                IQueryable<MasterSubKelompok> data = result.Where(d => d.MasterSubKelompokId == id);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MasterSubKelompok>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterSubKelompok>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterSubKelompok
        /// Menggunakan Kelompok ID tertentu
        /// </summary>
        /// <param name="kelompokId">ID Kelompok barang dalam bentuk angka</param>
        /// <returns>List item Sub kelompok barang</returns>
        public IList<MasterSubKelompok> GetByKelompokId(int kelompokId)
        {
            try
            {
                DbSet<MasterSubKelompok> result = db.MasterSubKelompok;
                IQueryable<MasterSubKelompok> data = result.Where(d => d.MasterKelompokId == kelompokId);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MasterSubKelompok>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterSubKelompok>();

            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterSubKelompok
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item Sub kelompok barang dalam bentuk string</param>
        /// <returns>List item Sub kelompok barang</returns>
        public IList<MasterSubKelompok> GetByNo(string no)
        {
            try
            {
                DbSet<MasterSubKelompok> result = db.MasterSubKelompok;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterSubKelompok>();
            }
        }
    }
}
