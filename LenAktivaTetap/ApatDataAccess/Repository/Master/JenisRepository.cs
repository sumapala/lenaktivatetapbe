﻿using ApatDataAccess.Models.Master;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ApatLog;
using System;

/// <summary>
/// Repository untuk Master Jenis Barang
/// </summary>
namespace ApatDataAccess.Repository.Master
{
    /// <summary>
    /// Class untuk repository Master Jenis Barang
    /// Berdasarkan interface IRepository
    /// Menggunakan model MasterJenis Barang
    /// </summary>
    public class JenisRepository : IRepository<MasterJenis>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Memasukan data dalam bentuk item ke database MasterJenis
        /// </summary>
        /// <param name="item">Item Jenis yang akan dimasukan berdasarkan model MasterJenis</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterJenis Insert(MasterJenis item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database MasterJenis
        /// </summary>
        /// <param name="item">Item Jenis yang akan diubah berdasarkan model MasterJenis</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public MasterJenis Update(MasterJenis item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database MasterJenis
        /// </summary>
        /// <param name="id">ID Item Jenis barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public MasterJenis Delete(int id)
        {
            try
            {
                MasterJenis item = db.MasterJenis.Find(id);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new MasterJenis();
            }
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database MasterJenis
        /// </summary>
        /// <returns>List item Jenis barang</returns>
        public IList<MasterJenis> GetAll()
        {
            try
            {
                DbSet<MasterJenis> result = db.MasterJenis;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterJenis>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterJenis
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item Jenis barang dalam bentuk angka</param>
        /// <returns>List item Jenis barang</returns>
        public IList<MasterJenis> GetById(int id)
        {
            try
            {
                DbSet<MasterJenis> result = db.MasterJenis;
                IQueryable<MasterJenis> data = result.Where(d => d.MasterJenisId == id);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MasterJenis>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterJenis>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterJenis
        /// Menggunakan Jenis ID tertentu
        /// </summary>
        /// <param name="subKelompokId">ID Jenis barang dalam bentuk angka</param>
        /// <returns>List item Jenis barang</returns>
        public IList<MasterJenis> GetBySubKelompokId(int subKelompokId)
        {
            try
            {
                DbSet<MasterJenis> result = db.MasterJenis;
                IQueryable<MasterJenis> data = result.Where(d => d.MasterSubKelompokId == subKelompokId);
                if (data != null)
                {
                    return data.ToList();
                }
                return new List<MasterJenis>();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterJenis>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database MasterJenis
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item Jenis barang dalam bentuk string</param>
        /// <returns>List item Jenis barang</returns>
        public IList<MasterJenis> GetByNo(string no)
        {
            try
            {
                DbSet<MasterJenis> result = db.MasterJenis;
                return result.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<MasterJenis>();
            }
        }
    }
}
