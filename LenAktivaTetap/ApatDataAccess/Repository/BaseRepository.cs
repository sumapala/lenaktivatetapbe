﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ApatDataAccess
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        ApatContext db = null;
        DbSet<T> table = null;

        public BaseRepository()
        {
            db = ApatContext.Connect();
            table = db.Set<T>();
        }

        public T Insert(T entity)
        {
            table.Add(entity);
            db.SaveChanges();
            return entity;
        }

        public T Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
            return entity;

        }
        public T Delete(int id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
            return existing;
        }

        public IList<T> GetAll()
        {
            return table.ToList;
        }

        public T GetById(int id)
        {
            return table.Find(id);
        }

        public T GetByNo(string no)
        {
            return 
        }


    }
}
