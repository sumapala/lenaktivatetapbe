﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;
using ApatLog;

namespace ApatDataAccess
{
    public class DiatRepository : IRepository<Diat>
    {
        private ApatContext db = ApatContext.Connect();

        public Diat Insert(Diat item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                Console.WriteLine(e.Message);
                return item;
            }
        }

        public Diat Update(Diat item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public Diat Delete(int id)
        {
            var item = db.Diat.Find(id);
            db.Remove(item);
            return item;
        }

        public IList<Diat> GetAll()
        {
            var res = db.Diat.Include(f => f.DiatDetail);
            return res.ToList();
        }

        public IList<Diat> GetById(int id)
        {
            var res = db.Diat.Include(f => f.DiatDetail);
            return res.Where(d => d.DiatId == id).ToList();
        }

        //public IList<Diat> GetByNo(string no)
        //{
        //    var res = db.Diat.Include(f => f.DiatDetail);
        //    return res.Where(d => d.NoDiat.Equals(no)).ToList();
        //}

        public string GetDocumentNumber()
        {
            int seq = 1;
            string docNo = "";

            string sDate = DateTime.Now.ToString();
            DateTime DocDate = DateTime.Now;
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int mn = Int32.Parse(datevalue.Month.ToString());
            int yy = Int32.Parse(datevalue.Year.ToString());

            try
            {
                var res = (from t in db.Diat orderby t.DiatId descending select t).FirstOrDefault();
                seq = res.DiatId + 1;
            }
            catch (InvalidOperationException)
            {

            }

            docNo = seq.ToString("D3") + "/DIAT/GL/" + GeneralUtility.ToRomanNumeral(mn) + "/" + yy.ToString();

            return docNo;


        }
    }
}
