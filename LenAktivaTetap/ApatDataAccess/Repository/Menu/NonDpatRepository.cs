﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;

namespace ApatDataAccess
{
    public class NonDpatRepository : IRepository<NonDpat>
    {
        private ApatContext db = ApatContext.Connect();

        public NonDpat Insert(NonDpat item)
        {
            db.Add(item);
            db.SaveChanges();
            return item;
        }

        public NonDpat Update(NonDpat item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public NonDpat Delete(int id)
        {
            var item = db.NonDpat.Find(id);
            db.Remove(item);
            return item;
        }

        public IList<NonDpat> GetAll()
        {
            var res = db.NonDpat.Include(f => f.NonDpatDetail);
            return res.ToList();
        }

        public IList<NonDpat> GetById(int id)
        {
            var res = db.NonDpat.Include(f => f.NonDpatDetail);
            return res.Where(d => d.NonDpatId == id).ToList();
        }

        public IList<NonDpat> GetByNo(string no)
        {
            var res = db.NonDpat.Include(f => f.NonDpatDetail);
            return res.Where(d => d.NoNonDpat.Equals(no)).ToList();
        }

        public string GetDocumentNumber()
        {
            int seq = 1;
            string docNo = "";

            string sDate = DateTime.Now.ToString();
            DateTime DocDate = DateTime.Now;
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int mn = Int32.Parse(datevalue.Month.ToString());
            int yy = Int32.Parse(datevalue.Year.ToString());

            try
            {
                var res = (from t in db.NonDpat orderby t.NonDpatId descending select t).FirstOrDefault();
                seq = res.NonDpatId + 1;
            }
            catch (InvalidOperationException)
            {

            }

            docNo = seq.ToString("D3") + "/BABH/GL/" + GeneralUtility.ToRomanNumeral(mn) + "/" + yy.ToString();

            return docNo;


        }
    }
}
