﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;

namespace ApatDataAccess
{
    public class DpatRepository : IRepository<Dpat>
    {
        private ApatContext db = ApatContext.Connect();

        public Dpat Insert(Dpat item)
        {
            db.Add(item);
            db.SaveChanges();
            return item;
        }

        public Dpat Update(Dpat item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public Dpat Delete(int id)
        {
            var item = db.Dpat.Find(id);
            item.Status = 99;
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public IList<Dpat> GetAll()
        {
            var res = db.Dpat.Include(f => f.DpatDetail);
            return res.ToList();
        }

        public IList<Dpat> GetById(int id)
        {
            var res = db.Dpat.Include(f => f.DpatDetail);
            return res.Where(d => d.DpatId == id).ToList();
        }

        public IList<Dpat> GetByNo(string no)
        {
            var res = db.Dpat.Include(f => f.DpatDetail);
            return res.Where(d => d.NoDpat.Equals(no)).ToList();
        }

        public string GetDocumentNumber()
        {
            int seq = 1;
            string docNo = "";

            string sDate = DateTime.Now.ToString();
            DateTime DocDate = DateTime.Now;
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int mn = Int32.Parse(datevalue.Month.ToString());
            int yy = Int32.Parse(datevalue.Year.ToString());

            try
            {
                var res = (from t in db.Dpat orderby t.DpatId descending select t).FirstOrDefault();
                seq = res.DpatId + 1;
            }
            catch(InvalidOperationException)
            {
                
            }

            docNo = seq.ToString("D3") + "/DPAT/GL/" + GeneralUtility.ToRomanNumeral(mn) + "/" + yy.ToString();

            return docNo;


        }
    }
}
