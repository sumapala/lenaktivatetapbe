﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;

namespace ApatDataAccess
{
    public class DiiRepository : IRepository<Dii>
    {
        private ApatContext db = ApatContext.Connect();

        public Dii Insert(Dii item)
        {
            db.Add(item);
            db.SaveChanges();
            return item;
        }

        public Dii Update(Dii item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }

        public Dii Delete(int id)
        {
            var item = db.Dii.Find(id);
            db.Remove(item);
            return item;
        }

        public IList<Dii> GetAll()
        {
            var res = db.Dii;
            return res.ToList();
        }

        public IList<Dii> GetById(int id)
        {
            var res = db.Dii;
            return res.Where(d => d.DiiId == id).ToList();
        }

        public IList<Dii> GetByNo(string no)
        {
            var res = db.Dii;
            return res.Where(d => d.NoInventaris.Equals(no)).ToList();
        }

        public IList<Dii> GetByFpfId(int FpfId, int UnitKerjaId)
        {
            var res = db.Dii;
            return res.Where(d => d.FpfId == FpfId && d.UnitKerjaId == UnitKerjaId).ToList();
        }

        //public IList<Dii> GetByUnitKerja
    }
}
