﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApatDataAccess
{
    public interface IRepository<T>
        where T : class
    {
        T Insert(T entity);
        T Update(T entity);
        T Delete(int id);

        //TODO Berbenturan dengan join yang bersifat dinamis
        //IList<T> GetAll();
        //IList<T> GetById(int id);
        //IList<T> GetByNo(string no);
    }
}
