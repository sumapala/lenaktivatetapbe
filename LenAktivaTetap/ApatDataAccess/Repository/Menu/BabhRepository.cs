﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;
using ApatLog;

/// <summary>
/// Namespace Apat Data Access
/// </summary>
namespace ApatDataAccess
{
    /// <summary>
    /// Class untuk repository BABH
    /// Berdasarkan interface IRepository
    /// Menggunakan model BABH
    /// </summary>
    public class BabhRepository : IRepository<Babh>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Fungsi simpan BABH
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public Babh Insert(Babh item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }

        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database Babh
        /// </summary>
        /// <param name="item">Item kelompok yang akan diubah berdasarkan model Babh</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public Babh Update(Babh item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }

        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database BABH
        /// </summary>
        /// <param name="id">ID Item BABH dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public Babh Delete(int id)
        {
            try
            {
                Babh item = db.Babh.Find(id);
                item.BabhStatusId = 99;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new Babh();
            }

        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database BABH
        /// </summary>
        /// <returns>List item kelompok barang</returns>
        public IList<Babh> GetAll()
        {
            try
            {
                DbSet<Babh> res = db.Babh;
                return res.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Babh>();
            }

        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database BABH
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item BABH dalam bentuk angka</param>
        /// <returns>List item BABH</returns>
        public IList<Babh> GetById(int id)
        {
            try
            {
                var res = db.Babh;
                return res.Where(d => d.BabhId == id).ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Babh>();
            }

        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database BABH
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item BABH dalam bentuk string</param>
        /// <returns>List item BABH</returns>
        public IList<Babh> GetByNo(string no)
        {
            try
            {
                var res = db.Babh;
                return res.Where(d => d.NoBabh.Equals(no)).ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Babh>();
            }
        }

        /// <summary>
        /// Fungsi untuk melakukan generate nomor dokumen
        /// </summary>
        /// <returns>string nomor dokumen</returns>
        public string GetDocumentNumber()
        {
            int seq = 1;
            string docNo = "";

            string sDate = DateTime.Now.ToString();
            DateTime DocDate = DateTime.Now;
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int mn = Int32.Parse(datevalue.Month.ToString());
            int yy = Int32.Parse(datevalue.Year.ToString());

            try
            {
                var res = (from t in db.Babh orderby t.BabhId descending select t).FirstOrDefault();
                seq = res.BabhId + 1;
            }
            catch (InvalidOperationException)
            {

            }

            docNo = seq.ToString("D3") + "/BABH/GL/" + GeneralUtility.ToRomanNumeral(mn) + "/" + yy.ToString();

            return docNo;


        }
    }
}
