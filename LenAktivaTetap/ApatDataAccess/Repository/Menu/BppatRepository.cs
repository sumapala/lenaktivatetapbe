﻿using System;
using System.Collections.Generic;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;
using Microsoft.EntityFrameworkCore;
using ApatLog;
using Microsoft.EntityFrameworkCore.Query;

/// <summary>
/// Namespace Repository
/// </summary>
namespace ApatDataAccess.Repository
{
    /// <summary>
    /// Class untuk repository Bpat
    /// Berdasarkan interface IRepository
    /// Menggunakan model Bpat
    /// </summary>
    public class BppatRepository : IRepository<Bppat>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Fungsi simpan Bppat
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public Bppat Insert(Bppat item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database Bppat
        /// </summary>
        /// <param name="item">Item kelompok yang akan diubah berdasarkan model Bpat</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public Bppat Update(Bppat item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database Bppat
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public Bppat Delete(int id)
        {
            try
            {
                Bppat item = db.Bppat.Find(id);
                item.BppatStatusId = 99;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new Bppat();
            }

        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database Bppat
        /// </summary>
        /// <returns>List item kelompok barang</returns>
        public IList<Bppat> GetAll()
        {
            try
            {
                IIncludableQueryable<Bppat, ICollection<BppatDetail>> res = db.Bppat.Include(f => f.BppatDetail);
                return res.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Bppat>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database Bppat
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>List item BPAT</returns>
        public IList<Bppat> GetById(int id)
        {
            try
            {
                IIncludableQueryable<Bppat, ICollection<BppatDetail>> res = db.Bppat.Include(f => f.BppatDetail);
                return res.Where(d => d.BppatId == id).ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Bppat>();
            }

        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database Bppat
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item BPAT dalam bentuk string</param>
        /// <returns>List item BPAT</returns>
        public IList<Bppat> GetByNo(string no)
        {
            try
            {
                var res = db.Bppat.Include(f => f.BppatDetail);
                return res.Where(d => d.NoBppat.Equals(no)).ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Bppat>();
            }

        }

        /// <summary>
        /// Fungsi untuk melakukan generate nomor dokumen
        /// </summary>
        /// <returns>string nomor dokumen</returns>
        public string GetDocumentNumber()
        {
            int seq = 1;
            string docNo = "";

            string sDate = DateTime.Now.ToString();
            DateTime DocDate = DateTime.Now;
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int mn = Int32.Parse(datevalue.Month.ToString());
            int yy = Int32.Parse(datevalue.Year.ToString());

            try
            {
                var res = (from t in db.Bpat orderby t.BpatId descending select t).FirstOrDefault();
                seq = res.BpatId + 1;
            }
            catch (InvalidOperationException)
            {

            }

            docNo = seq.ToString("D3") + "/BPPAT/GL/" + GeneralUtility.ToRomanNumeral(mn) + "/" + yy.ToString();

            return docNo;
        }

        /// <summary>
        /// Menghapus data detail dalam bentuk item pada database Bppat
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public BppatDetail UpdateDetail(BppatDetail item)
        {
            try
            {
                db.Entry(item).State = EntityState.Deleted;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Fungsi simpan Bppat
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public BppatDetail InsertDetail(BppatDetail item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Fungsi cek keberadaan detail item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsDetailExist(int id)
        {
            return db.BppatDetail.Count(e => e.BppatId == id) > 0;
        }

        /// <summary>
        /// Fungsi ambil detail dengan BppatId tertentu
        /// </summary>
        /// <param name="id"</param>
        /// <returns></returns>
        public IQueryable<BppatDetail> GetItemFromBppat(int id)
        {
            return db.BppatDetail.Where(e => e.BppatId == id);
        }
    }
}
