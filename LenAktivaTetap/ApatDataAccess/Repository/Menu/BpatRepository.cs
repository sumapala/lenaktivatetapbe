﻿using System;
using System.Collections.Generic;
using System.Text;
using ApatDataAccess.Models;
using System.Linq;
using ApatDataAccess.Helpers;
using Microsoft.EntityFrameworkCore;
using ApatLog;
using Microsoft.EntityFrameworkCore.Query;

/// <summary>
/// Namespace Repository
/// </summary>
namespace ApatDataAccess.Repository
{
    /// <summary>
    /// Class untuk repository Bpat
    /// Berdasarkan interface IRepository
    /// Menggunakan model Bpat
    /// </summary>
    public class BpatRepository:IRepository<Bpat>
    {
        /// <summary>
        /// Instance database
        /// Berdasarkan context ApatContext
        /// </summary>
        private ApatContext db = ApatContext.Connect();

        /// <summary>
        /// Fungsi simpan Bpat
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public Bpat Insert(Bpat item)
        {
            try
            {
                db.Add(item);
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                Console.WriteLine(e.ToString());
                return item;
            }
        }

        /// <summary>
        /// Mengubah data dalam bentuk item pada database Bpat
        /// </summary>
        /// <param name="item">Item kelompok yang akan diubah berdasarkan model Bpat</param>
        /// <returns>Item yang telah disimpan, sama dengan item yang di request</returns>
        public Bpat Update(Bpat item)
        {
            try
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return item;
            }
        }

        /// <summary>
        /// Menghapus data dalam bentuk item pada database Bpat
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>Item yang telah dihapus</returns>
        public Bpat Delete(int id)
        {
            try
            {
                Bpat item = db.Bpat.Find(id);
                item.BpatStatusId = 99;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new Bpat();
            }
            
        }

        /// <summary>
        /// Mengambil semua data dalam bentuk List item pada database Bpat
        /// </summary>
        /// <returns>List item kelompok barang</returns>
        public IList<Bpat> GetAll()
        {
            try
            {
                IIncludableQueryable<Bpat, ICollection<BpatDetail>> res = db.Bpat.Include(f => f.BpatDetail);
                return res.ToList();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<Bpat>();
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database BPAT
        /// Menggunakan ID tertentu
        /// </summary>
        /// <param name="id">ID Item kelompok barang dalam bentuk angka</param>
        /// <returns>List item BPAT</returns>
        public IList<Bpat> GetById(int id)
        {
            try
            {
                IIncludableQueryable<Bpat, ICollection<BpatDetail>> res = db.Bpat.Include(f => f.BpatDetail);
                return res.Where(d => d.BpatId == id).ToList();
            }
            catch(Exception e)
            {
                e.SaveToFile();
                return new List<Bpat>();
            }
            
        }

        /// <summary>
        /// Mengambil data dalam bentuk List item pada database BPAT
        /// Menggunakan no tertentu
        /// </summary>
        /// <param name="no">No Item BPAT dalam bentuk string</param>
        /// <returns>List item BPAT</returns>
        public IList<Bpat> GetByNo(string no)
        {
            try
            {
                var res = db.Bpat.Include(f => f.BpatDetail);
                return res.Where(d => d.NoBpat.Equals(no)).ToList();
            }
            catch(Exception e)
            {
                e.SaveToFile();
                return new List<Bpat>();
            }
            
        }

        /// <summary>
        /// Fungsi untuk melakukan generate nomor dokumen
        /// </summary>
        /// <returns>string nomor dokumen</returns>
        public string GetDocumentNumber()
        {
            int seq = 1;
            string docNo = "";

            string sDate = DateTime.Now.ToString();
            DateTime DocDate = DateTime.Now;
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int mn = Int32.Parse(datevalue.Month.ToString());
            int yy = Int32.Parse(datevalue.Year.ToString());

            try
            {
                var res = (from t in db.Bpat orderby t.BpatId descending select t).FirstOrDefault();
                seq = res.BpatId + 1;
            }
            catch (InvalidOperationException)
            {

            }

            docNo = seq.ToString("D3") + "/BPAT/GL/" + GeneralUtility.ToRomanNumeral(mn) + "/" + yy.ToString();

            return docNo;
        }
    }
}
