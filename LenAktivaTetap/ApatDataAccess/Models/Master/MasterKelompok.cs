﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Model untuk Master Kelompok Barang
/// </summary>
namespace ApatDataAccess.Models.Master
{
    /// <summary>
    /// Class yang digunakan untuk Master Kelompok Barang
    /// </summary>
    [Table("masterkelompok")]
    public class MasterKelompok
    {
        /// <summary>
        /// ID Kelompok Barang
        /// Juga berfungsi sebagai Key untuk model Master Kelompok Barang
        /// </summary>
        [Key]
        public int MasterKelompokId { get; set; }

        /// <summary>
        /// Nama Kelompok Barang
        /// </summary>
        public string NamaMasterKelompok { get; set; }
    }
}
