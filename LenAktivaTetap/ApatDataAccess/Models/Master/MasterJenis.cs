﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Model untuk Master Jenis Barang
/// </summary>
namespace ApatDataAccess.Models.Master
{
    /// <summary>
    /// Class yang digunakan untuk Master Jenis Barang
    /// </summary>
    [Table("masterjenis")]
    public class MasterJenis
    {
        /// <summary>
        /// ID Jenis Barang
        /// Juga berfungsi sebagai Key untuk model Master Jenis Barang
        /// </summary>
        [Key]
        public int MasterJenisId { get; set; }

        /// <summary>
        /// ID Sub Kelompok Barang
        /// </summary>
        public int MasterSubKelompokId { get; set; }

        /// <summary>
        /// Nama Jenis Barang
        /// </summary>
        public string NamaMasterJenis { get; set; }
    }
}
