﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

/// <summary>
/// Namespace Model
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Model class User Role
    /// </summary>
    [Table("userrole")]
    public class UserRole
    {
        /// <summary>
        /// User Role ID Primary Key
        /// </summary>
        public int UserRoleId { get; set; }
        /// <summary>
        /// User ID
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Role ID
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// Unit Kerja ID
        /// </summary>
        public int UnitKerjaId { get; set; }
    }
}
