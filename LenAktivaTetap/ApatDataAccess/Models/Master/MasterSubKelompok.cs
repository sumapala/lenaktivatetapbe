﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Model untuk Master Sub Kelompok Barang
/// </summary>
namespace ApatDataAccess.Models.Master
{
    /// <summary>
    /// Class yang digunakan untuk Master Sub Kelompok Barang
    /// </summary>
    [Table("mastersubkelompok")]
    public class MasterSubKelompok
    {
        /// <summary>
        /// ID Sub Kelompok Barang
        /// Juga berfungsi sebagai Key untuk model Master Sub Kelompok Barang
        /// </summary>
        [Key]
        public int MasterSubKelompokId { get; set; }
        
        /// <summary>
        /// ID Kelompok Barang
        /// </summary>
        public int MasterKelompokId { get; set; }

        /// <summary>
        /// Nama Sub Kelompok Barang
        /// </summary>
        public string NamaMasterSubKelompok { get; set; }
    }
}
