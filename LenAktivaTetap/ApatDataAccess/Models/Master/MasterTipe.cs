﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Model untuk Master Tipe Barang
/// </summary>
namespace ApatDataAccess.Models.Master
{
    /// <summary>
    /// Class yang digunakan untuk Master Tipe Barang
    /// </summary>
    [Table("mastertipe")]
    public class MasterTipe
    {
        /// <summary>
        /// ID Tipe Barang
        /// Juga berfungsi sebagai Key untuk model Master Tipe Barang
        /// </summary>
        [Key]
        public int MasterTipeId { get; set; }

        /// <summary>
        /// ID Jenis Barang
        /// </summary>
        public int MasterJenisId { get; set; }

        /// <summary>
        /// Nama Tipe Barang
        /// </summary>
        public string NamaMasterTipe { get; set; }

        /// <summary>
        /// Kode Tipe Barang
        /// </summary>
        public string KodeMasterTipe { get; set; }
    }
}
