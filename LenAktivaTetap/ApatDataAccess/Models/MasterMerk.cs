﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApatDataAccess.Models
{
    [Table("mastermerk")]
    public class MasterMerk
    {
        [Key]
        public int MerkId { get; set; }
        public string MerkNo { get; set; }
        public string NamaMerk { get; set; }
    }
}
