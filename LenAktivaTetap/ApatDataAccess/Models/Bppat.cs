﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ApatDataAccess.Models
{
    [Table("bppat")]
    public class Bppat
    {
        [Key]
        public int BppatId { get; set; }
        public DateTime? CreatedDate { get;set;}
        public DateTime? TanggalPeminjaman { get; set; }
        public DateTime? TanggalPengembalian { get; set; }
        public DateTime? RencanaKembali { get; set; }
        public string NoBppat { get; set; }
        public int? JenisBppat { get; set; }
        public string NamaKegiatan { get; set; }
        public string KodeKegiatan { get; set; }
        public string Kebutuhan { get; set; }
        public int? UnitKerjaIdPeminjam { get; set; }
        public int? UnitKerjaIdPemberi { get; set; }
        public string KodeUnitKerjaPeminjam { get; set; }
        public string KodeUnitKerjaPemberi { get; set; }
        public string NamaUnitKerjaPeminjam { get; set; }
        public string NamaUnitKerjaPemberi { get; set; }
        public int? UserIdFpfPeminjam { get; set; }
        public int? UserIdFpfPemberi { get; set; }
        public string NikFpfPeminjam { get; set; }
        public string NikFpfPemberi { get; set; }
        public string NamaFpfPeminjam { get; set; }
        public string NamaFpfPemberi { get; set; }
        public int? UserIdKaPeminjam1 { get; set; }
        public int? UserIdKaPemberi1 { get; set; }
        public string NikKaPeminjam1 { get; set; }
        public string NikKaPemberi1 { get; set; }
        public string NamaKaPeminjam1 { get; set; }
        public string NamaKaPemberi1 { get; set; }
        public int? UserIdKaPeminjam2 { get; set; }
        public int? UserIdKaPemberi2 { get; set; }
        public string NikKaPeminjam2 { get; set; }
        public string NikKaPemberi2 { get; set; }
        public string NamaKaPeminjam2 { get; set; }
        public string NamaKaPemberi2 { get; set; }
        public int? ApprKaPeminjam1 { get; set; }
        public int? ApprKaPemberi1 { get; set; }
        public int? ApprKaPeminjam2 { get; set; }
        public int? ApprKaPemberi2 { get; set; }
        public int? BppatStatusId { get; set; }
        public virtual ICollection<BppatDetail> BppatDetail { get; set; }

    }
}
