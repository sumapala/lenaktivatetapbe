﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApatDataAccess.Models
{
    [Table("mappingaktiva")]
    public class MappingAktiva
    {
        [Key]
        public int MappingId { get; set; }
        public int FpfId { get; set; }
        public int UnitKerjaId { get; set; }
    }
}