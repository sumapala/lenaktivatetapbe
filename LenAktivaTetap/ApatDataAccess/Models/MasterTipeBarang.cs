﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApatDataAccess.Models
{
    [Table("mastertipebarang")]
    public class MasterTipeBarang
    {
        public int TipeId { get; set; }
        public string TipeNo { get; set; }
        public string NamaTipe { get; set; }
    }
}
