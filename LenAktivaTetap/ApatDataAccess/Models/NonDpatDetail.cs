﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApatDataAccess.Models
{
    [Table("nondpatdetail")]
    public class NonDpatDetail
    {
        [Key]
        public int NonDpatDetailId { get; set; }
        public string NoNonDpat { get; set; }
        public string MerkType { get; set; }
        public string NoSeri { get; set; }
        public string MataAnggaran { get; set; }
        public string NoInventaris { get; set; }
        public float JumlahNilai { get; set; }
        public string JumlahSatuan { get; set; }
        public float NilaiPerolehan { get; set; }
        public DateTime TanggalPerolehan { get; set; }
        public string Keterangan { get; set; }

    }
}
