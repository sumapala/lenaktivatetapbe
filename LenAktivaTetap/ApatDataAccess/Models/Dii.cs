﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Namespace Apat Data Access
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Class Model DII
    /// </summary>
    [Table("dii")]
    public class Dii
    {
        /// <summary>
        /// DII ID yang merupakan primary key
        /// </summary>
        public int DiiId { get; set; }
        /// <summary>
        /// No urut DII
        /// </summary>
        public int NoUrut { get; set; }
        /// <summary>
        /// ID Kelompok Barang
        /// </summary>
        public string KelompokBarang { get; set; }
        /// <summary>
        /// Nama Barang
        /// </summary>
        public string NamaAktivaTetap { get; set; }
        /// <summary>
        /// Jenis Barang
        /// </summary>
        public string JenisBarang { get; set; }
        /// <summary>
        /// Kelompok Pemakaian
        /// </summary>
        public string KelompokPemakaian { get; set; }
        /// <summary>
        /// Tipe Barang
        /// </summary>
        public string NamaTipe { get; set; }
        /// <summary>
        /// Merk Barang
        /// </summary>
        public string NamaMerk { get; set; }
        /// <summary>
        /// No Seri
        /// </summary>
        public string NoSeri { get; set; }
        /// <summary>
        /// No Inventaris Lama
        /// </summary>
        public string NoInventaris { get; set; }
        /// <summary>
        /// No Inventaris Lama
        /// </summary>
        public string NoInventaris1 { get; set; }
        /// <summary>
        /// No Inventaris Lama
        /// </summary>
        public string NoInventaris2 { get; set; }
        /// <summary>
        /// No Inventaris Lama
        /// </summary>
        public string NoInventaris3 { get; set; }
        /// <summary>
        /// No Inventaris Baru
        /// </summary>
        public string NoInventaris4 { get; set; }
        /// <summary>
        /// No Inventaris Baru
        /// </summary>
        public string NoInventaris5 { get; set; }
        /// <summary>
        /// Tahun Beli
        /// </summary>
        public int TahunBeli { get; set; }
        /// <summary>
        /// Kondisi
        /// </summary>
        public string Kondisi { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Keterangan
        /// </summary>
        public string Keterangan { get; set; }
        /// <summary>
        /// Deskripsi
        /// </summary>
        public string Deskripsi { get; set; }
        /// <summary>
        /// Nomor BAPB
        /// </summary>
        public string NoBapb { get; set; }
        /// <summary>
        /// Nomor DPAT
        /// </summary>
        public string NoDpat { get; set; }
        /// <summary>
        /// Lokasi ID
        /// </summary>
        public string LokasiId { get; set; }
        /// <summary>
        /// Lokasi Kantor
        /// </summary>
        public string LokasiKantor { get; set; }
        /// <summary>
        /// Tanggal Beli
        /// </summary>
        public DateTime? TanggalBeli { get; set; }
        /// <summary>
        /// Mata Uang Beli
        /// </summary>
        public string MataUangBeli { get; set; }
        /// <summary>
        /// Harga Beli
        /// </summary>
        public double? NilaiPerolehan { get; set; }
        /// <summary>
        /// Model Barang
        /// </summary>
        public string ModelBarang { get; set; }
        /// <summary>
        /// Nomor Polisi
        /// </summary>
        public string NoPolisi { get; set; }
        /// <summary>
        /// Nomor Sertifikasi
        /// </summary>
        public string NoSertifikat { get; set; }
        /// <summary>
        /// Tanggal Sertifikat
        /// </summary>
        public DateTime? TanggalSertifikat { get; set; }
        /// <summary>
        /// Nomor Surat BPN
        /// </summary>
        public string NoSuratBpn { get; set; }
        /// <summary>
        /// Tanggal Surat BPN
        /// </summary>
        public DateTime? TanggalSuratBpn { get; set; }
        /// <summary>
        /// Luas
        /// </summary>
        public double? Luas { get; set; }
        /// <summary>
        /// Mata Uang Revaluasi
        /// </summary>
        public string MataUangRevaluasi { get; set; }
        /// <summary>
        /// Nilai Revaluasi
        /// </summary>
        public double? NilaiRevaluasi { get; set; }
        /// <summary>
        /// FPF ID
        /// </summary>
        public int FpfId { get; set; }
        /// <summary>
        /// Unit Kerja ID
        /// </summary>
        public int UnitKerjaId { get; set; }
        /// <summary>
        /// DII Status ID
        /// </summary>
        public int DiiStatusId { get; set; }
        /// <summary>
        /// Nama Kelompok Barang
        /// </summary>
        public string NamaKelompokBarang { get; set; }
        /// <summary>
        /// Mapping ID
        /// </summary>
        public int? MappingId { get; set; }
    }
}
