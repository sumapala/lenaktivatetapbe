﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
/// <summary>
/// Namespace Apat Data Access
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Class model Lokasi Kantor
    /// </summary>
    public class LokasiKantor
    {
        /// <summary>
        /// Lokasi Kantor ID yang merupakan Primary Key
        /// </summary>
        [Key]
        public int LokasiKantorId { get; set; }
        /// <summary>
        /// Lokasi Kantor
        /// </summary>
        public string LokasiKantorNo { get; set; }
        /// <summary>
        /// Nama Kantor
        /// </summary>
        public string Nama { get; set; }
    }
}
