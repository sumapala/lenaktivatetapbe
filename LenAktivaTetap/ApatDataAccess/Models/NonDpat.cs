﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApatDataAccess.Models
{
    [Table("nondpat")]
    public class NonDpat
    {
        [Key]
        public int NonDpatId { get; set; }
        public string NoNonDpat { get; set; }
        public string Bapb { get; set; }
        public DateTime TanggalNonDpat { get; set; }
        public int ApprLogistik { get; set; }
        public string NikLogistik { get; set; }
        public int ApprUmum { get; set; }
        public string NikUmum { get; set; }
        public ICollection<NonDpatDetail> NonDpatDetail { get; set; }


    }
}
