﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApatDataAccess.Models
{
    [Table("dpatdetail")]
    public class DpatDetail
    {
        [Key]
        public int DpatDetailId { get; set; }
        public string NoDpat { get; set; }
        public int DpatId { get; set; }
        public string Merk { get; set; }
        public string Tipe { get; set; }
        public string NamaBarang { get; set; }
        public string NoSeri { get; set; }
        public string MataAnggaran { get; set; }
        public string NoInventaris { get; set; }
        public int Jumlah { get; set; }
        public string Satuan { get; set; }
        public string HargaBeli { get; set; }
        public string TanggalBeli { get; set; }
        public string Keterangan { get; set; }
    }
}
