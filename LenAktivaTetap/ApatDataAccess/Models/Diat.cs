﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Namespace Apat Data Access
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Class Model DIAT
    /// </summary>
    [Table("diat")]
    public class Diat
    {
        /// <summary>
        /// Diat ID yang merupakan primary key
        /// </summary>
        [Key]
        public int DiatId { get; set; }
        /// <summary>
        /// Periode
        /// </summary>
        public string Periode { get; set; }
        /// <summary>
        /// Lokasi Nomor Ruangan
        /// </summary>
        public string LokasiNomorRuangan { get; set; }
        /// <summary>
        /// Penanggung Jawab
        /// </summary>
        public string PenanggungJawab { get; set; }
        /// <summary>
        /// Unit Kerja
        /// </summary>
        public int? UnitKerjaId { get; set; }
        /// <summary>
        /// NIK FPF
        /// </summary>
        public string NikFpf { get; set; }
        /// <summary>
        /// Approval FPF
        /// </summary>
        public string NamaFpf { get; set; }
        /// <summary>
        /// NIK Kepala Unit Kerja
        /// </summary>
        public string NikKaUnitKerja { get; set; }
        /// <summary>
        /// NIK Kepala Unit Kerja
        /// </summary>
        public string NamaKaUnitKerja { get; set; }
        /// <summary>
        /// UserId Kepala Unit Kerja
        /// </summary>
        public int UserIdKaUnitKerja { get; set; }
        /// <summary>
        /// Approval Kepala Unit Kerja
        /// </summary>
        public int ApprKaUnitKerja { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// KodeUnitKerja
        /// </summary>
        public string KodeUnitKerja { get; set; }
        /// <summary>
        /// KodeUnitKerja
        /// </summary>
        public int UserIdFpf { get; set; }
        /// <summary>
        /// NamaUnitKerja
        /// </summary>
        public string NamaUnitKerja { get; set; }
        /// <summary>
        /// DiatStatusId
        /// </summary>
        public int DiatStatusId { get; set; }
        /// <summary>
        /// Collection DIAT Detail
        /// </summary>
        public ICollection<DiatDetail> DiatDetail { get; set; }
    }
}
