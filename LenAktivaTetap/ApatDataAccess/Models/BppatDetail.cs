﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApatDataAccess.Models
{
    public class BppatDetail
    {
        public int BppatDetailId { get; set; }
        public int BppatId { get; set; }
        public int DiiId { get; set; }
        public string NamaAktivaTetap { get; set; }
        public string NamaMerk { get; set; }
        public string NamaTipe { get; set; }
        public string NoSeri { get; set; }
        public string NoInventaris { get; set; }
        public string KondisiPinjam { get; set; }
        public string KondisiKembali { get; set; }
    }
}
