﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApatDataAccess.Models
{
    [Table("dpat")]
    public class Dpat
    {

        [Key]
        public int DpatId { get; set; }
        public string NoDpat { get; set; }
        public string Bapb { get; set; }
        public string NamaSupplier { get; set; }
        public string TanggalDpat { get; set; }
        public string NamaLogistik { get; set; }
        public string NikLogistik { get; set; }
        public string NamaUmum { get; set; }
        public string NikUmum { get; set; }
        public int Status { get; set; }
        public ICollection<DpatDetail> DpatDetail { get; set; }

    }
}
