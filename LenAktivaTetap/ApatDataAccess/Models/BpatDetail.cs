﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// model class
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Class untuk model BPAT Detail
    /// </summary>
    [Table("bpatdetail")]
    public class BpatDetail
    {
        /// <summary>
        /// BpatDetailId Primary Key
        /// </summary>
        [Key]
        public int BpatDetailId { get; set; }
        /// <summary>
        /// DII Id
        /// </summary>
        public int DiiId { get; set; }
        /// <summary>
        /// BPAT ID, foreign key
        /// </summary>
        public int BpatId { get; set; }
        /// <summary>
        /// Nama Aktiva Tetap
        /// </summary>
        public string NamaAktivaTetap { get; set; }
        /// <summary>
        /// Nama Merk
        /// </summary>
        public string NamaMerk { get; set; }
        /// <summary>
        /// Nama Tipe
        /// </summary>
        public string NamaTipe { get; set; }
        /// <summary>
        /// Nomor Seri
        /// </summary>
        public string NoSeri { get; set; }
        /// <summary>
        /// Nomor Inventaris Lama
        /// </summary>
        public string NoInventaris { get; set; }
        /// <summary>
        /// Nomor Inventaris Baru
        /// </summary>
        public string NoInventaris1 { get; set; }
        /// <summary>
        /// Kondisi Barang
        /// </summary>
        public string Kondisi { get; set; }
        /// <summary>
        /// Keterangan
        /// </summary>
        public string Keterangan { get; set; }
    }
}
