﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApatDataAccess.Models
{
    [Table("diatdetail")]
    public class DiatDetail
    {
        [Key]
        public int DiatDetailId { get; set; }
        public int DiiId { get; set; }
        public int DiatId { get; set; }
        public string NamaAktivaTetap { get; set; }
        public string NamaMerk { get; set; }
		public string NamaTipe { get; set; }
        public string NoInventaris { get; set; }
        public string NoInventaris1 { get; set; }
        public string NoDpat { get; set; }
        public string TahunBeli { get; set; }
        public string Kondisi { get; set; }
        public string Status { get; set; }
        public string Keterangan { get; set; }
    }
}
