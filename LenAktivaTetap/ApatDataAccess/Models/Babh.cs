﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Namespace Models
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Class model untuk BABH
    /// </summary>
    [Table("babh")]
    public class Babh
    {
        /// <summary>
        /// BABH ID merupakan primary key
        /// </summary>
        [Key]
        public int BabhId { get; set; }
        /// <summary>
        /// Nomor BABH
        /// </summary>
        public string NoBabh { get; set; }
        /// <summary>
        /// Tanggal BABH
        /// </summary>
        public DateTime? TanggalBabh { get; set; }
        /// <summary>
        /// Nama Pelapor
        /// </summary>
        public string Nama { get; set; }
        /// <summary>
        /// NIK
        /// </summary>
        public string Nik { get; set; }
        /// <summary>
        /// KTP
        /// </summary>
		public string KTP { get; set; }
        /// <summary>
        /// Alamat
        /// </summary>
        public string Alamat { get; set; }
        /// <summary>
        /// Laporan
        /// </summary>
        public string Laporan { get; set; }
        /// <summary>
        /// Lokasi Hilang
        /// </summary>
        public string LokasiHilang { get; set; }
        /// <summary>
        /// Tanggal Hilang
        /// </summary>
        public DateTime? TanggalHilang { get; set; }
        //public string NamaPelapor { get; set; }
        //public string NikPelapor { get; set; }
        //public string NamaDireksi { get; set; }
        /// <summary>
        /// Status Dokumen BABH
        /// </summary>
        public int? BabhStatusId { get; set; }
        /// <summary>
        /// FPF ID
        /// </summary>
        public int? FpfId { get; set; }
        /// <summary>
        /// NIK FPF
        /// </summary>
        public string NikFpf { get; set; }
        /// <summary>
        /// Nama FPF
        /// </summary>
        public string NamaFpf { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        public DateTime? CreatedDate { get; set; }

    }
}
