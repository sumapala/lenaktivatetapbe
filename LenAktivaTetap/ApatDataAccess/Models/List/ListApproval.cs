﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Kumpulan model untuk List
/// </summary>
namespace ApatDataAccess.Models.List
{
    /// <summary>
    /// List Approval untuk persetujuan peminjaman barang
    /// </summary>
    [Table("listapproval")]
    public class ListApproval
    {
        /// <summary>
        /// ID approval
        /// </summary>
        [Key]
        public int ListApprovalId { get; set; }

        /// <summary>
        /// User yang men-submit peminjaman barang
        /// </summary>
        public int SubmitterId { get; set; }

        /// <summary>
        /// user approval pertama yang akan mengizinkan proses peminjaman
        /// </summary>
        public int ApproverId1 { get; set; }

        /// <summary>
        /// User approval kedua yang akan mengizinkan proses peminjaman
        /// </summary>
        public int ApproverId2 { get; set; }

        /// <summary>
        /// Menu yang menyangkut proses peminjaman
        /// </summary>
        public int MenuId { get; set; }

        /// <summary>
        /// Dokumen yang menyangkut proses peminjaman
        /// </summary>
		public int DocumentId { get; set; }

        /// <summary>
        /// Status User approval pertama yang mengizinkan proses peminjaman
        /// </summary>
        public int ApprovalStatusApproverId1 { get; set; }

        /// <summary>
        /// Status User approval kedua yang mengizinkan proses peminjaman
        /// </summary>
        public int ApprovalStatusApproverId2 { get; set; }

        /// <summary>
        /// Status approval pada kondisi saat ini (0 atau 99)
        /// 0 berarti masih bisa diakses
        /// 99 berarti sudah tidak bisa diakses
        /// </summary>
        public int approvalStatusId { get; set; }

        /// <summary>
        /// Waktu pembuatan approval
        /// </summary>
        public DateTime TimeStamp { get; set; }

    }
}
