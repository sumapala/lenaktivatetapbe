﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Data Log untuk aplikasi Apat
/// 1. Error Log dalam bentuk file
/// 2. Audit Trail (perubahan data)
/// 3. Log Trail (aktivitas user)
/// </summary>
namespace ApatDataAccess.Models.Logs
{
    /// <summary>
    /// Model untuk Audit Trail (perubahan data)
    /// </summary>
    [Table("logaudittrail")]
    public class LogAuditTrail
    {
        /// <summary>
        /// Id audit trail
        /// </summary>
        [Key]
        public int AuditTrailId { get; set; }

        /// <summary>
        /// User ID yang melakukan perubahan
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Tipe aksi yang akan direkam ketika logging
        /// Added = 4
        /// Deleted = 2
        /// Modified = 3
        /// </summary>
        public int AuditActionType { get; set; }

        /// <summary>
        /// Model yang mendapatkan dampak ketika ada perubahan
        /// </summary>
        public string DataModel { get; set; }

        /// <summary>
        /// Data model sebelum terjadi perubahan
        /// </summary>
        public string ValueBefore { get; set; }

        /// <summary>
        /// Data model setelah terjadi perubahan
        /// </summary>
        public string ValueAfter { get; set; }

        /// <summary>
        /// Waktu ketika terjadi logging
        /// </summary>
        public DateTime DateTimeStamp { get; set; }
    }
}
