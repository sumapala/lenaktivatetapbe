﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Model Class
/// </summary>
namespace ApatDataAccess.Models
{
    /// <summary>
    /// Class yang digunakan untuk BPAT
    /// </summary>
    [Table("bpat")]
    public class Bpat
    {
        /// <summary>
        /// Primary Key BpatId
        /// </summary>
        [Key]
        public int BpatId { get; set; }
        /// <summary>
        /// NoBpat
        /// </summary>
        public string NoBpat { get; set; }
        /// <summary>
        /// Lokasi dan Nomor Ruangan
        /// </summary>
        public string LokasiNomorRuangan { get; set; }
        /// <summary>
        /// Penanggung Jawab
        /// </summary>
        public string PenanggungJawab { get; set; }
        /// <summary>
        /// Unit Kerja ID Pemberi
        /// </summary>
        public int? UnitKerjaIdPemberi { get; set; }
        /// <summary>
        /// Nama Unit Kerja Id Pemberi
        /// </summary>
        public string NamaUnitKerjaPemberi { get; set; }
        /// <summary>
        /// Tanggal BPAT
        /// </summary>
        public DateTime TanggalBpat { get; set; }
        /// <summary>
        /// NIK FPF Pemberi
        /// </summary>
        public string NikFPFPemberi { get; set; }
        /// <summary>
        /// NIK FPF Penerima
        /// </summary>
        public string NikFPFPenerima { get; set; }
        /// <summary>
        /// Nama FPF Pemberi
        /// </summary>
        public string NamaFPFPemberi { get; set; }
        /// <summary>
        /// Nama FPF Penerima
        /// </summary>
        public string NamaFPFPenerima { get; set; }
        /// <summary>
        /// NIK Kepala Unit Kerja Pemberi
        /// </summary>
        public string NikKaPemberi { get; set; }
        /// <summary>
        /// NIK Kepala Unit Kerja Penerima
        /// </summary>
        public string NikKaPenerima { get; set; }
        /// <summary>
        /// Nama Kepala Unit Kerja Pemberi
        /// </summary>
        public string NamaKaPemberi { get; set; }
        /// <summary>
        /// Nama Kepala Unit Kerja Penerima 
        /// </summary>
        public string NamaKaPenerima { get; set; }
        /// <summary>
        /// Status Approval Kepala Unit Kerja Pemberi
        /// </summary>
        public int? ApprKaPemberi { get; set; }
        /// <summary>
        /// Status Approval Kepala Unit Kerja Penerima
        /// </summary>
        public int? ApprKaPenerima { get; set; }
        /// <summary>
        /// Status Dokumen BPAT
        /// </summary>
        public int? BpatStatusId { get; set; }
        /// <summary>
        /// ID FPF Pemberi
        /// </summary>
        public int? FpfIdPemberi { get; set; }
        /// <summary>
        /// ID FPF Penerima
        /// </summary>
        public int? FpfIdPenerima { get; set; }
        /// <summary>
        /// ID Unit Kerja Penerima
        /// </summary>
        public int? UnitKerjaIdPenerima { get; set; }
        /// <summary>
        /// Nama Unit Kerja Penerima
        /// </summary>
        public string NamaUnitKerjaPenerima { get; set; }
        /// <summary>
        /// Kepala Pemberi
        /// </summary>
        public int? KaIdPemberi { get; set; }
        /// <summary>
        /// Kepala Penerima
        /// </summary>
        public int? KaIdPenerima { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// Collection Detail dari BPAT
        /// </summary>
        public virtual ICollection<BpatDetail> BpatDetail { get; set; }
    }
}
