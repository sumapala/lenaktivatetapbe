﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using ApatDataAccess.Models;
using ApatDataAccess.Helpers;
using ApatDataAccess.Transactions;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System.Xml;
using System.IO;
using System.Reflection;

namespace ApatDataAccess
{
    public class Program
    {

        //private static readonly log4net.ILog log =
        //log4net.LogManager.GetLogger(typeof(Program));
        /// <summary>
        /// Fungsi untuk pengetesan saja
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            //var entry = new DpatMaster()
            //{
            //    NoDpat = "DPATGANTENG",
            //    Bapb = "123345",
            //    TanggalDpat = "123345",
            //    ApprLogistik = 0,
            //    NikLogistik = "123345",
            //    ApprUmum = 0,
            //    NikUmum = "123345"
            //};

            //using (var context = ApatContext.Connect(Constant.ConnectionString))
            //{
            //var item = context.Dpat.FindAsync(4);
            //if(item == null)
            //{

            //}
            //else
            //{
            //    try
            //    {
            //        context.Dpat.Remove(item);
            //        context.SaveChangesAsync();
            //    }
            //    catch (DbUpdateConcurrencyException e)
            //    {
            //        Console.WriteLine(e.Message);
            //    }
            //}
            //}

            //DpatTransaction dpat = new DpatTransaction();
            //var x = dpat.Delete(4);

            //Console.WriteLine(x);
            //XmlDocument log4netConfig = new XmlDocument();
            //log4netConfig.Load(File.OpenRead("log4net.config"));

            //var repo = log4net.LogManager.CreateRepository(
            //    Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            //log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
            //log.Info("Application - Main is invoked");

            

            BabhRepository babh = new BabhRepository();
            var entry = new Babh()
            {
                NoBabh = "DPATGANTENG",
                TanggalBabh = null,
                Nama = "123345",
                Nik = "12345",
                KTP = "123345",
                Alamat = "Jl. Panjunan No. 9 Bandung",
                Laporan = "Kehilangan apapun itu yang ada di diri saya sendiri",
                LokasiHilang = "Len Subang",
                TanggalHilang =  null,
                BabhStatusId = 1,
                FpfId =  1,
                NikFpf = "18271872817",
                NamaFpf =  "saya",
            };

            try
            {
                babh.Insert(entry);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.InnerException);
            }
            
            
            Console.ReadKey();
        }
    }
}