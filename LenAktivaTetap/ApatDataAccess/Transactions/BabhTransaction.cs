﻿using System;
using System.Collections.Generic;
using System.Text;
using ApatDataAccess.Models;
using ApatDataAccess;
using MySql.Data.MySqlClient;

namespace ApatDataAccess.Transactions
{
    public class BabhTransaction
    {
        MySqlConnection connection = new MySqlConnection
        {
            ConnectionString = Constant.ConnectionString
        };

        public bool Delete(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM babh WHERE BabhId = @BabhId;", connection);
                command.Parameters.AddWithValue("@BabhId", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                connection.Close();
                return false;
            }

            

        }

    }
}