﻿using System;
using System.Collections.Generic;
using System.Text;

using ApatDataAccess.Models;
using ApatDataAccess;
using MySql.Data.MySqlClient;

namespace ApatDataAccess.Transactions
{
    public class NonDpatTransaction
    {
        MySqlConnection connection = new MySqlConnection
        {
            ConnectionString = Constant.ConnectionString
        };

        public bool Delete(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM nondpat WHERE NonDpatId = @NonDpatId;", connection);
                command.Parameters.AddWithValue("@NonDpatId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool DeleteDetail(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM nondpatdetail WHERE NonDpatDetailId = @NonDpatDetailId;", connection);
                command.Parameters.AddWithValue("@NonDpatDetailId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }
    }
}
