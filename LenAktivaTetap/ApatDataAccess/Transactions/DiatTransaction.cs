﻿using System;
using System.Collections.Generic;
using System.Text;

using ApatDataAccess.Models;
using ApatDataAccess;
using MySql.Data.MySqlClient;

namespace ApatDataAccess.Transactions
{
    public class DiatTransaction
    {
        MySqlConnection connection = new MySqlConnection
        {
            ConnectionString = Constant.ConnectionString
        };

        public bool Delete(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM diat WHERE DiatId = @DiatId;", connection);
                command.Parameters.AddWithValue("@DiatId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool DeleteDetail(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM diatdetail WHERE DiatDetailId = @DiatDetailId;", connection);
                command.Parameters.AddWithValue("@DiatDetailId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }
    }
}
