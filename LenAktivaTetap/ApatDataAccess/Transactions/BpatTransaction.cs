﻿using System;
using System.Collections.Generic;
using System.Text;
using ApatDataAccess.Models;
using ApatDataAccess;
using MySql.Data.MySqlClient;

namespace ApatDataAccess.Transactions
{
    public class BpatTransaction
    {
        MySqlConnection connection = new MySqlConnection
        {
            ConnectionString = Constant.ConnectionString
        };

        public bool Delete(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM bpat WHERE DpatId = @BpatId;", connection);
                command.Parameters.AddWithValue("@BpatId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool DeleteDetail(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM bpatdetail WHERE BpatDetailId = @BpatDetailId;", connection);
                command.Parameters.AddWithValue("@BpatDetailId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

    }
}
