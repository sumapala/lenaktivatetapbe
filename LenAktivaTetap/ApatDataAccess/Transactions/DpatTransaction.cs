﻿using System;
using System.Collections.Generic;
using System.Text;
using ApatDataAccess.Models;
using ApatDataAccess;
using MySql.Data.MySqlClient;

namespace ApatDataAccess.Transactions
{
    public class DpatTransaction
    {
        private static readonly log4net.ILog log =
        log4net.LogManager.GetLogger(typeof(DpatTransaction));

        MySqlConnection connection = new MySqlConnection
        {
            ConnectionString = Constant.ConnectionString
        };

        public bool Delete(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM dpat WHERE DpatId = @DpatId;", connection);
                command.Parameters.AddWithValue("@DpatId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool DeleteDetail(int id)
        {
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM dpatdetail WHERE DpatDetailId = @DpatDetailId;", connection);
                command.Parameters.AddWithValue("@DpatDetailId", id);
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

    }
}
