﻿using ApatDataAccess.Models;
using ApatDataAccess.Models.Logs;
using ApatDataAccess.Models.Master;
using ApatDataAccess.Models.List;
using ApatLog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using MySQL.Data.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Pembentukan context pada namespace ApatDataAccess
/// </summary>
namespace ApatDataAccess
{
    /// <summary>
    /// Class ApatContext sebagai context yang merujuk ke DbContext
    /// </summary>
    public class ApatContext : DbContext
    {
        /// <summary>
        /// Fungsi untuk membentuk context dengan connString
        /// </summary>
        /// <returns>ApatContext</returns>
        public static ApatContext Connect()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            string connectionString = configuration.GetConnectionString("ApatDbConnection");


            DbContextOptionsBuilder<ApatContext> optionsBuilder = new DbContextOptionsBuilder<ApatContext>();
            optionsBuilder.UseMySQL(connectionString);
            
            ApatContext context = new ApatContext(optionsBuilder.Options);
            context.Database.EnsureCreated();

            return context;
        }

        /// <summary>
        /// Objek Context
        /// </summary>
        /// <param name="options">Opsi yang merujuk pada class ApatContext yang akan digunakan pada DB Context</param>
        public ApatContext(DbContextOptions<ApatContext>options)
            : base(options)
        {

        }

        /// <summary>
        /// Interaksi antara Table Database dan DPAT
        /// </summary>
        public DbSet<Dpat> Dpat { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan Detail DPAT
        /// </summary>
        public DbSet<DpatDetail> DpatDetail { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan BABH
        /// </summary>
        public DbSet<Babh> Babh { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan BPAT
        /// </summary>
        public DbSet<Bpat> Bpat { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan Detail BPAT
        /// </summary>
        public DbSet<BpatDetail> BpatDetail { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan DIAT
        /// </summary>
        public DbSet<Diat> Diat { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan Detail DIAT
        /// </summary>
        public DbSet<DiatDetail> DiatDetail { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan Non DPAT
        /// </summary>
        public DbSet<NonDpat> NonDpat { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan Detail Non DPAT
        /// </summary>
        public DbSet<NonDpatDetail> NonDpatDetail { get; set; }

        /// <summary>
        /// Interaksi antara Table Database dan DII
        /// </summary>
        public DbSet<Dii> Dii { get; set; }
        
        /// <summary>
        /// Interaksi antara Database dan Role App
        /// </summary>
        public DbSet<RoleApp> Role { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Master Merk
        /// </summary>
        public DbSet<MasterMerk> MasterMerk { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Lokasi Kantor
        /// </summary>
        public DbSet<LokasiKantor> LokasiKantor { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Master Kelompok Barang
        /// </summary>
        public DbSet<MasterKelompok> MasterKelompok { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Master Sub Kelompok Barang
        /// </summary>
        public DbSet<MasterSubKelompok> MasterSubKelompok { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Master Jenis Barang
        /// </summary>
        public DbSet<MasterJenis> MasterJenis { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Master Tipe Barang
        /// </summary>
        public DbSet<MasterTipe> MasterTipe { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Audit Trail
        /// </summary>
        public DbSet<LogAuditTrail> LogAuditTrail { get; set; }

        /// <summary>
        /// Interaksi antara Database dan ListApproval
        /// </summary>
        public DbSet<ListApproval> ListApproval { get; set; }

        /// <summary>
        /// Interaksi antara Database dan Bppat
        /// </summary>
        public DbSet<Bppat> Bppat { get; set; }

        /// <summary>
        /// Interaksi antara Database dan BppatDetail
        /// </summary>
        public DbSet<BppatDetail> BppatDetail { get; set; }

        /// <summary>  
        /// Overriding Save Changes dari db context
        /// </summary>  
        /// <returns>1 jika Sukses dan -1 jika gagal</returns>
        public override int SaveChanges()
        {
            try
            {
                List<LogAuditTrail> auditTrailList = AuditTrail.Save(ChangeTracker.Entries());
                if (auditTrailList.Count() > 0)
                {
                    foreach (LogAuditTrail item in auditTrailList)
                    {
                        base.Add(item);
                    }
                }
                return base.SaveChanges();
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return -1;
            }
        }

        /// <summary>
        /// Interaksi antara Database dan MappingAktiva
        /// </summary>
        public DbSet<MappingAktiva> MappingAktiva { get; set; }

        /// <summary>
        /// Interaksi antara Database dan userRole
        /// </summary>
        public DbSet<UserRole> UserRole { get; set; }
    }
}
