﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ApatDataAccess
{
    public static class PagingUtils
    {
        /// <summary>
        /// Len Result jika IEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rs"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static Object Page<T>(this IEnumerable<T> rs, int? pageNo = 1, int? pageSize = 5)
        {
            IEnumerable<T> res = Enumerable.Empty<T>();
            var xtotal = rs.Count();
            if (pageSize > 0)
            {
                res = rs.Skip((pageNo.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }
            else
            {
                res = rs;
            }
            var pgsize = pageSize > 0 ? pageSize.Value : xtotal;
            var pgCount = xtotal > 0 ? (int)Math.Ceiling(xtotal / (double)pgsize) : 0;

            return new
            {
                Result = res,
                PageNo = pageNo,
                PageSize = pageSize,
                PageCount = xtotal > 0 ? (int)Math.Ceiling(xtotal / (double)pgsize) : 0,
                Total = xtotal
            };
        }

        /// <summary>
        /// Len Result jika IQueryable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rs"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static Object Page<T>(this IQueryable<T> rs, int? pageNo = 1, int? pageSize = 0)
        {
            IQueryable<T> res = null;
            var xtotal = rs.Count();
            if (pageSize > 0)
            {
                res = rs.Skip((pageNo.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }
            else
            {
                res = rs;
            }
            var pgsize = pageSize > 0 ? pageSize.Value : xtotal;
            var pgCount = xtotal > 0 ? (int)Math.Ceiling(xtotal / (double)pgsize) : 0;

            return new
            {
                Result = res,
                PageNo = pageNo,
                PageSize = pageSize,
                PageCount = xtotal > 0 ? (int)Math.Ceiling(xtotal / (double)pgsize) : 0,
                Total = xtotal
            };
        }

        /// <summary>
        /// Len Result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rs"></param>
        /// <param name="message"></param>
        /// <param name="status"></param>
        /// <returns>Object</returns>
        public static Object Result<T>(this IEnumerable<T> rs, string message, int status, int offset = 0)
        {
            IEnumerable<T> res = Enumerable.Empty<T>();
            var xtotal = rs.Count();
            
            return new
            {
                Status = status,
                Message = message,
                Data = rs.Skip(offset).Take(10),
                Total = xtotal
            };
            
        }

        /// <summary>
        /// Len Result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rs"></param>
        /// <param name="message"></param>
        /// <param name="status"></param>
        /// <returns>Object</returns>
        public static Object Result<T>(this IQueryable<T> rs, string message, int status, int offset = 0)
        {
            //IQueryable<T> res = null;
            var xtotal = rs.Count();

            return new
            {
                Status = status,
                Message = message,
                Data = rs.Skip(offset).Take(10),
                Total = xtotal
            };

        }
    }
}