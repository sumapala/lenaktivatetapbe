﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApatDataAccess
{
    public class ResponseObject
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public object Total { get; set; }

        //public ResponseObject(string message, int code, JArray data)
        //{
        //    return new ResponseObject { Message = message, Code = code, Data = data };
        //}
    }
}
