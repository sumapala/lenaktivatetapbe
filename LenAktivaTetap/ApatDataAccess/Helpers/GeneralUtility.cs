﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApatDataAccess.Helpers
{
    public class GeneralUtility
    {
        /// <summary>
        /// Konversi Integer ke Romawi
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRomanNumeral(int number)
        {
            var romanNumeral = string.Empty;
            while (number > 0)
            {
                var index = Constant.numerals.FindIndex(x => x <= number);
                number -= Constant.numerals[index];
                romanNumeral += Constant.romanNumerals[index];
            }
            return romanNumeral;
        }
    }
}
