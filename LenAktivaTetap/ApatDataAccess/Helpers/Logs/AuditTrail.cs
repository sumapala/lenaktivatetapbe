﻿using ApatDataAccess.Models;
using ApatDataAccess.Models.Logs;
using ApatLog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

/// <summary>
/// Audit trail on ApatDataAccess
/// </summary>
namespace ApatDataAccess
{
    /// <summary>
    /// 2. Audit Trail (perubahan data)
    /// </summary>
    public class AuditTrail
    {
        /// <summary>
        /// Konstanta untuk akses string Apat data model
        /// </summary>
        private const string APAT_MODEL_NAMESPACE = "ApatDataAccess.Models";

        /// <summary>
        /// Save Audit Trail to Database
        /// </summary>
        /// <param name="entries">Entries dari ChangeTracker</param>
        /// <returns>List dari LogAuditTrail</returns>
        public static List<LogAuditTrail> Save(IEnumerable<EntityEntry> entries)
        {
            try
            {
                List<LogAuditTrail> listModel = new List<LogAuditTrail>();
                IEnumerable<EntityEntry> objects = entries
                    .Where(p => p.State == EntityState.Added ||
                        p.State == EntityState.Deleted ||
                        p.State == EntityState.Modified);
                DateTime now = DateTime.Now;
                foreach (var item in objects)
                {
                    LogAuditTrail model;
                    model = ExtractClass(item.Entity, item.State);
                    if(model != null)
                    {
                        model.AuditActionType = (int)item.State;
                        model.DateTimeStamp = now;
                        // TODO change userId
                        model.UserId = "Public";
                    }
                    listModel.Add(model);
                }
                return listModel;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new List<LogAuditTrail>();
            }
        }

        /// <summary>
        /// Ekstraks data dari entries
        /// </summary>
        /// <param name="entityEntry">Content Entries yang akan di ekstrak</param>
        /// <param name="entityState">State aksi yang sedang terjadi</param>
        /// <returns>Model AuditTrailModel</returns>
        private static LogAuditTrail ExtractClass(object entityEntry, EntityState entityState)
        {
            try
            {
                List<Type> getClass = GetClasses();
                Type entityType = entityEntry.GetType();
                LogAuditTrail model = new LogAuditTrail();
                foreach (Type itemClass in getClass)
                {
                    if (entityType == itemClass)
                    {
                        model.DataModel = entityType.FullName;
                        if (entityState == EntityState.Added)
                        {
                            model.ValueAfter = JsonConvert.SerializeObject(entityEntry);
                        }
                    }
                }
                return model;
            }
            catch (Exception e)
            {
                e.SaveToFile();
                return new LogAuditTrail();
            }
        }

        /// <summary>
        /// Mendapatkan Properties dari Namespace
        /// </summary>
        /// <returns>List dari Type Properties</returns>
        private static List<Type> GetClasses()
        {
            Assembly asm = typeof(Dpat).GetTypeInfo().Assembly;
            List<Type> namespacelist = new List<Type>();
            List<Type> classlist = new List<Type>();
            foreach (Type type in asm.GetTypes())
            {
                if (type.Namespace == APAT_MODEL_NAMESPACE)
                    namespacelist.Add(type);
            }
            foreach (Type classname in namespacelist)
                classlist.Add(classname);
            return classlist;
        }
    }

    /// <summary>
    /// Testing file untuk development
    /// </summary>
    public static class TestingSaveFile
    {
        /// <summary>
        /// Save string ke file
        /// </summary>
        /// <param name="data">String yang akan di save</param>
        public static void SaveFile(string data)
        {
            string rootDirectory = @"c:\Apat\Log\";
            string fileName = "ApatTesting - ";
            if (!Directory.Exists(rootDirectory))
            {
                Directory.CreateDirectory(rootDirectory);
            }
            using (StreamWriter sw = File.AppendText(rootDirectory + fileName + ".log"))
            {
                sw.WriteLine(data);
            }
        }
    }
}
