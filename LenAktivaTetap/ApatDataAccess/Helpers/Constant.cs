﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApatDataAccess
{
    /// <summary>
    /// Class untuk menyimpan konstanta
    /// </summary>
    public static class Constant
    {
        public const string ConnectionString = "server=localhost;userid=root;pwd=;port=3306;database=lenat;sslmode=none;";
        public static List<string> romanNumerals = new List<string>() { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
        public static List<int> numerals = new List<int>() { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };


    }
}
