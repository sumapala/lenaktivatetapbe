using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;
using System.IO;
using ApatLog;
using ApatDataAccess.Models.Logs;

namespace ApatWebApi.Controllers
{
    [EnableCors("ApatPolicy")]
    [Produces("application/json")]
    [Route("api/Dpat")]
    public class DpatController : Controller
    {
        ResponseObject r = new ResponseObject();
        DpatRepository dpat = new DpatRepository();
        //TrailRepository trail = new TrailRepository();

        // GET: api/Dpat
        /// <summary>
        /// Get All DPAT Master Data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetDpat(string offset)
        {
            try
            {
                var res = dpat.GetAll();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // GET: api/Dpat/5
        /// <summary>
        /// Get DPAT Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDpatById", Name = "GetDpatById")]
        public IActionResult GetDpatById(int dpatId)
        {
            try
            {
                var res = dpat.GetById(dpatId);
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res.FirstOrDefault(), Total = res.Count() });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/Dpat/5
        /// <summary>
        /// Get DPAT Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDpatByNo", Name = "GetDpatByNo")]
        public IActionResult GetByNo(string dpatNo)
        {
            try
            {
                var res = dpat.GetByNo(dpatNo);
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // POST: api/Dpat
        /// <summary>
        /// Insert DPAT with JArray param from Angular2
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Dpat> listData = arrInput.ToObject<List<Dpat>>();

            foreach (var item in listData)
            {
                try
                {
                    dpat.Insert(item);
                    //trail.Insert(new LogAuditTrail() { UserId = "public", DataModel = "Dpat", AuditActionType = 1, DateTimeStamp=DateTime.Now });
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

                //TestingSaveFile.SaveFile("MASUK SINI");
            }

            return Ok(new ResponseObject { Status = 200, Message = "Insert Success", Data = null });

        }

        // PUT: api/Dpat/5
        /// <summary>
        /// Update Dpat
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<Dpat> listData = arrInput.ToObject<List<Dpat>>();
            var res = new Dpat();

            foreach (var item in listData)
            {
                try
                {
                    res = dpat.Update(item);
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res });
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete DPAT tanpa context
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
           
            foreach(var id in arrInput)
            {
                dpat.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = null });
        }

        /// <summary>
        /// GetDocumentNumber
        /// </summary>
        /// <returns></returns>
        [Route("GetDpatNumber", Name ="GetDpatNumber")]
        [HttpGet]
        public IActionResult GetDocumentNumber()
        {
            try
            {
                var docNo = dpat.GetDocumentNumber();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = docNo });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
    }
}
