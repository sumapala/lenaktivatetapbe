﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;

/// <summary>
/// Namespace Controller
/// </summary>
namespace ApatWebApi.Controllers
{
    /// <summary>
    /// Class Controller DII
    /// </summary>
    [EnableCors("ApatPolicy")]
    [Route("api/[controller]")]
    public class DiiController : Controller
    {
        /// <summary>
        /// Inisiasi objek Response
        /// </summary>
        ResponseObject r = new ResponseObject();
        DiiRepository dii = new DiiRepository();

        // GET: api/dii
        /// <summary>
        /// Get All dii Master Data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetDii(int offset)
        {
            try
            {
                var res = dii.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // GET: api/dii/5
        /// <summary>
        /// getAtListByFpf
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetAtListByFpf", Name = "GetAtListByFpf")]
        public IActionResult GetAtListByFpf(int FpfId, int UnitKerjaId)
        {
            try
            {
                //var res = dii.GetByFpfId(FpfId, UnitKerjaId);
                var res = dii.GetAll();
                return Ok(res.Result("Success", 200, 30));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/dii/5
        /// <summary>
        /// Get dii Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDiiById", Name = "GetDiiById")]
        public IActionResult GetdiiById(int DiiId)
        {
            try
            {
                var res = dii.GetById(DiiId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/dii/5
        /// <summary>
        /// Get dii Detail by KodeInventaris
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDiiByNo", Name = "GetDiiByNo")]
        public IActionResult GetByNo(string KodeInventaris)
        {
            try
            {
                var res = dii.GetByNo(KodeInventaris);
                //return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/dii/5
        /// <summary>
        /// Get dii Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDiiByFpfId", Name = "GetDiiByFpfId")]
        public IActionResult GetDiiByFpfId(string FpfId, int UnitKerjaId)
        {
            int id = int.Parse(FpfId);

            try
            {
                var res = dii.GetByFpfId(id, UnitKerjaId);
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // POST: api/dii
        /// <summary>
        /// Insert dii with JArray param from Angular2
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Dii> listData = arrInput.ToObject<List<Dii>>();

            foreach (var item in listData)
            {
                try
                {
                    dii.Insert(item);
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            return Ok(new ResponseObject { Status = 200, Message = "Insert Success", Data = null });

        }

        // PUT: api/dii/5
        /// <summary>
        /// Update dii
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<Dii> listData = arrInput.ToObject<List<Dii>>();
            var res = new Dii();
            var allOk = false;
            Exception x = new Exception();

            foreach (var item in listData)
            {
                try
                {
                    res = dii.Update(item);
                    allOk = true;
                    //return Ok(item);
                }
                catch (Exception e)
                {
                    x = e;
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            if(allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res });
            }
            else
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = x });
            }
            
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete dii tanpa context
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {

            foreach (var id in arrInput)
            {
                dii.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = null });
        }
    }
}
