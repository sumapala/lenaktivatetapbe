using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/NonDpatDetail")]
    public class NonDpatDetailController : Controller
    {
        ApatContext db = ApatContext.Connect();
        List<ResponseObject> r = new List<ResponseObject>();

        /// <summary>
        /// Get All
        /// </summary>
        /// <returns></returns>
        // GET: api/NonDpatDetail
        [HttpGet]
        public IActionResult GetNonDpatDetail()
        {
            var res = db.NonDpatDetail;
            return Ok(res);
        }

        /// <summary>
        /// Get Detail By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/NonDpatDetail/5
        [HttpGet("{id}", Name = "GetNonDpatDetail")]
        public IActionResult Get(int id)
        {
            var res = db.NonDpat.Where(f => f.NonDpatId == id);
            return Ok(res);
        }

        /// <summary>
        /// Insert Data
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // POST: api/NonDpatDetail
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]JArray arrInput)
        {
            List<NonDpatDetail> listData = arrInput.ToObject<List<NonDpatDetail>>();
            var allOk = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in listData)
                {
                    if (db.NonDpatDetail.Any(e => e.NonDpatDetailId == item.NonDpatDetailId))
                    {
                        allOk = 1;
                    }
                    else
                    {
                        db.NonDpatDetail.Add(item);
                        await db.SaveChangesAsync();

                    }
                }

                if (allOk == 0)
                {
                    transaction.Commit();
                    return Ok(db.NonDpatDetail);
                }
                else
                {
                    transaction.Rollback();
                    return BadRequest();
                }
            }

        }

        /// <summary>
        /// UPdate
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // PUT: api/NonDpatDetail/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody]JArray arrInput)
        {
            List<NonDpatDetail> listData = arrInput.ToObject<List<NonDpatDetail>>();
            var allOk = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in listData)
                {
                    if (!IsExists(item.NonDpatDetailId))
                    {
                        allOk = 1;
                    }
                    else
                    {
                        try
                        {
                            db.Entry(item).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                        catch (DbUpdateException e)
                        {
                            return Ok(e.Message);
                        }

                    }
                }

                if (allOk == 0)
                {
                    transaction.Commit();
                    return Ok(db.NonDpatDetail);
                }
                else
                {
                    transaction.Rollback();
                    return BadRequest();
                }


            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
            int allOk = 0;
            NonDpatTransaction nonDpatTrx = new NonDpatTransaction();

            bool res;

            foreach (var id in arrInput)
            {
                try
                {
                    res = nonDpatTrx.DeleteDetail(id);

                }
                catch (Exception)
                {
                    allOk = 0;
                }
            }

            if (allOk == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Cek Record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IsExists(int id)
        {
            return db.NonDpatDetail.Count(e => e.NonDpatDetailId == id) > 0;
        }
    }
}
