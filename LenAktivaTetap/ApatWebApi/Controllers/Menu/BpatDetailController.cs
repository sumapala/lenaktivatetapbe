using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/BpatDetail")]
    public class BpatDetailController : Controller
    {
        ApatContext db = ApatContext.Connect();
        List<ResponseObject> r = new List<ResponseObject>();

        /// <summary>
        /// GetBpatDetail
        /// </summary>
        /// <returns></returns>
        // GET: api/BpatDetail
        [HttpGet]
        public IActionResult GetBpatDetail()
        {
            var res = db.BpatDetail;
            return Ok(res);
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/BpatDetail/5
        [HttpGet("{id}", Name = "GetBpatDetail")]
        public IActionResult Get(int id)
        {
            var res = db.Bpat.Where(f => f.BpatId == id);
            return Ok(res);
        }


        /// <summary>
        /// POST
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // POST: api/BpatDetail
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]JArray arrInput)
        {
            List<BpatDetail> listData = arrInput.ToObject<List<BpatDetail>>();
            var allOk = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in listData)
                {
                    if (db.BpatDetail.Any(e => e.BpatDetailId == item.BpatDetailId))
                    {
                        allOk = 1;
                    }
                    else
                    {
                        db.BpatDetail.Add(item);
                        await db.SaveChangesAsync();

                    }
                }

                if (allOk == 0)
                {
                    transaction.Commit();
                    return Ok(db.BpatDetail);
                }
                else
                {
                    transaction.Rollback();
                    return BadRequest();
                }
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // PUT: api/BpatDetail/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]JArray arrInput)
        {
            List<BpatDetail> listData = arrInput.ToObject<List<BpatDetail>>();
            var allOk = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in listData)
                {
                    if (!IsExists(item.BpatDetailId))
                    {
                        allOk = 1;
                    }
                    else
                    {
                        try
                        {
                            db.Entry(item).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                        catch (DbUpdateException e)
                        {
                            return Ok(e.Message);
                        }

                    }
                }

                if (allOk == 0)
                {
                    transaction.Commit();
                    return Ok(db.BpatDetail);
                }
                else
                {
                    transaction.Rollback();
                    return BadRequest();
                }


            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
            int allOk = 0;
            BpatTransaction bpatTrx = new BpatTransaction();

            bool res;

            foreach (var id in arrInput)
            {
                try
                {
                    res = bpatTrx.DeleteDetail(id);

                }
                catch (Exception)
                {
                    allOk = 0;
                }
            }

            if (allOk == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Cek Record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IsExists(int id)
        {
            return db.BpatDetail.Count(e => e.BpatDetailId == id) > 0;
        }
    }
}
