using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Cors;
using ApatDataAccess.Repository;
using ApatLog;

/// <summary>
/// Namescape Controllers
/// </summary>
namespace ApatWebApi.Controllers
{
    /// <summary>
    /// Controller BPPAT
    /// </summary>
    [EnableCors("ApatPolicy")]
    [Produces("application/json")]
    [Route("api/Bppat")]
    public class BppatController : Controller
    {
        /// <summary>
        /// Instance Response Object
        /// </summary>
        ResponseObject r = new ResponseObject();
        /// <summary>
        /// Instance Repository BPAT
        /// </summary>
        BppatRepository bppat = new BppatRepository();
        /// <summary>
        /// Exception Trap
        /// </summary>
        Exception error = new Exception();
        Bppat response = new Bppat();
        BppatDetail modelDetail = new BppatDetail();

        // GET: api/Bppat
        /// <summary>
        /// Fungsi untuk mengambil seluruh data BPPAT
        /// </summary>
        /// <param name="offset">Index List data BPAT</param>
        /// <returns>JSON data BPAT, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetBppat(int offset)
        {
            try
            {
                IList<Bppat> res = bppat.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // GET: api/Bppat/5
        /// <summary>
        /// Mengambil data BPAT berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/bpat/GetBpatById?bpatId=1
        /// </summary>
        /// <param name="id">ID BPAT dalam bentuk angka</param>
        /// <returns>Item BPAT</returns>
        [HttpGet("GetBppatById", Name = "GetBppatById")]
        public IActionResult GetBpatById(int bppatId)
        {
            try
            {
                IList<Bppat> res = bppat.GetById(bppatId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/Bppat/5
        /// <summary>
        /// Mengambil data BPPAT berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/bpat/GetBpatByNo?bpatNo=1
        /// </summary>
        /// <param name="id">ID BPAT dalam bentuk angka</param>
        /// <returns>Item BPAT</returns>
        [HttpGet("GetBppatByNo", Name = "GetBppatByNo")]
        public IActionResult GetByNo(string bppatNo)
        {
            try
            {
                var res = bppat.GetByNo(bppatNo);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // POST: api/Bppat
        /// <summary>
        /// Menyimpan data BPPAT
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Bppat> listData = arrInput.ToObject<List<Bppat>>();
            
            bool allOk = false;

            foreach (Bppat item in listData)
            {
                
                try
                {
                    item.CreatedDate = DateTime.Now;
                    bppat.Insert(item);
                    response = item;
                    allOk = true;

                }
                catch (Exception e)
                {
                    error = e;
                    e.SaveToFile();
                }

            }

            if (allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = response.BppatId });
                //return Ok("its gona be ok");
            }
            else
            {
                return BadRequest(error);
            }

        }

        // PUT: api/Bppat/5
        /// <summary>
        /// Update Bppat
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<Bppat> listData = arrInput.ToObject<List<Bppat>>();
            List<BppatDetail> listDataDetail = arrInput[0]["bppatDetail"].ToObject<List<BppatDetail>>();

            bool allOk = false;

            foreach (var item in listData)
            {

                var fordelete = bppat.GetItemFromBppat(item.BppatId);
                foreach (BppatDetail todelete in fordelete)
                {
                    try
                    {
                        bppat.UpdateDetail(todelete);
                    }
                    catch (Exception e)
                    {
                        e.SaveToFile();
                        return Ok(e.InnerException);
                        //error = e.Message;
                    }
                }

                //foreach (BppatDetail itemdetail in listDataDetail)
                //{

                //    if (!bppat.IsDetailExist(itemdetail.BppatDetailId))
                //    {
                //        itemdetail.BppatId = item.BppatId;
                //        bppat.InsertDetail(itemdetail);
                //        //return Ok(itemdetail);
                //    }
                //    else
                //    {
                //        itemdetail.BppatId = item.BppatId;
                //        bppat.UpdateDetail(itemdetail);
                //        //return Ok(itemdetail);
                //    }
                //}

                //try
                //{
                //    bppat.Update(item);
                //    response = item;
                //    allOk = true;
                //}
                //catch (Exception e)
                //{
                //    error = e;
                //    e.SaveToFile();
                //}

            }

            if (allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = response });
                //return Ok("its gona be ok");
            }
            else
            {
                return BadRequest(error);
            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// API untuk hapus BPPAT
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {

            foreach (var id in arrInput)
            {
                bppat.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = arrInput });
        }

        /// <summary>
        /// API untuk melakaukan generate Nomor Dokumen
        /// </summary>
        /// <returns></returns>
        [Route("GetBppatNumber", Name = "GetBppatNumber")]
        [HttpGet]
        public IActionResult GetBpatNumber()
        {
            try
            {
                var docNo = bppat.GetDocumentNumber();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = docNo });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
    }
}
