using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Microsoft.AspNetCore.Cors;
using ApatLog;

/// <summary>
/// Namescape Controllers
/// </summary>
namespace ApatWebApi.Controllers
{
    /// <summary>
    /// Controller BABH
    /// </summary>
    [EnableCors("ApatPolicy")]
    [Produces("application/json")]
    [Route("api/Babh")]
    public class BabhController : Controller
    {
        /// <summary>
        /// Instance Response Object
        /// </summary>
        List<ResponseObject> r = new List<ResponseObject>();
        /// <summary>
        /// Instance Repository BABH
        /// </summary>
        BabhRepository babh = new BabhRepository();
        /// <summary>
        /// Exception Trap
        /// </summary>
        Exception error = new Exception();
        Babh response = new Babh();

        /// <summary>
        /// Fungsi untuk mengambil seluruh data BABH
        /// </summary>
        /// <param name="offset">Index List data BABH</param>
        /// <returns>JSON data BABH, dikirim ke client</returns>
        // GET: api/Babh
        [HttpGet]
        public IActionResult GetBabh(int offset)
        {
            try
            {
                var res = babh.GetAll();
                return Ok(res.Result("Success", 200, offset));
                //return Ok(res.Result(2));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// <summary>
        /// Mengambil data BABH berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/babh/GetBabhById?babhId=1
        /// </summary>
        /// <param name="id">ID BABH dalam bentuk angka</param>
        /// <returns>Item BABH</returns>
        // GET: api/Babh/5
        [HttpGet("GetBabhById", Name = "GetBabhById")]
        public IActionResult GetBabhById(int babhId)
        {
            try
            {
                var res = babh.GetById(babhId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
        
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // POST: api/Babh
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Babh> listData = arrInput.ToObject<List<Babh>>();
            bool allOk = false;


            foreach (var item in listData)
            {
                try
                {
                    item.CreatedDate = DateTime.Now;
                    babh.Insert(item);
                    response = item;
                    allOk = true;
                    
                }
                catch (Exception e)
                {
                    error = e;
                    e.SaveToFile();
                }

            }

            if(allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = response.BabhId });
            }
            else
            {
                return BadRequest(error);
            }

           

        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // PUT: api/Babh/5
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<Babh> listData = arrInput.ToObject<List<Babh>>();

            foreach (var item in listData)
            {
                try
                {
                    babh.Update(item);
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            return Ok(new ResponseObject { Status = 200, Message = "Update Success", Data = null });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
            foreach (var id in arrInput)
            {
                babh.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = null });
        }

        //private bool IsExists(int id)
        //{
        //    return db.Babh.Count(e => e.BabhId == id) > 0;
        //}

        /// <summary>
        /// GetDocumentNumber
        /// </summary>
        /// <returns></returns>
        [Route("GetBabhNumber", Name = "GetBabhNumber")]
        [HttpGet]
        public IActionResult GetBabhNumber()
        {
            try
            {
                var docNo = babh.GetDocumentNumber();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = docNo });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
    }
}
