using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;
using ApatLog;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/NonDpat")]
    public class NonDpatController : Controller
    {
        ResponseObject r = new ResponseObject();
        NonDpatRepository nondpat = new NonDpatRepository();
        NonDpat response = new NonDpat();
        Exception error = new Exception();

        // GET: api/Dpat
        /// <summary>
        /// Get All Non DPAT Master Data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetNonDpat(int offset)
        {
            try
            {
                IList<NonDpat> res = nondpat.GetAll();
                //return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // GET: api/Dpat/5
        /// <summary>
        /// Get Non DPAT Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetNonDpatById", Name = "GetNonDpatById")]
        public IActionResult GetNonDpatById(int nondpatId)
        {
            try
            {
                var res = nondpat.GetById(nondpatId);
                //return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/Dpat/5
        /// <summary>
        /// Get Non DPAT Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetNonDpatByNo", Name = "GetNonDpatByNo")]
        public IActionResult GetNonByNo(string nondpatNo)
        {
            try
            {
                var res = nondpat.GetByNo(nondpatNo);
                //return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // POST: api/Dpat
        /// <summary>
        /// Insert NonDPAT with JArray param from Angular2
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<NonDpat> listData = arrInput.ToObject<List<NonDpat>>();
            var allOk = false;

            foreach (var item in listData)
            {
                try
                {
                    //item.CreatedDate = DateTime.Now;
                    nondpat.Insert(item);
                    response = item;
                    allOk = true;

                }
                catch (Exception e)
                {
                    error = e;
                    e.SaveToFile();
                }

            }

            if (allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = response.NonDpatId });
            }
            else
            {
                return BadRequest(error);
            }

        }

        // PUT: api/Dpat/5
        /// <summary>
        /// Update Non Dpat
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<NonDpat> listData = arrInput.ToObject<List<NonDpat>>();
            var res = new NonDpat();

            foreach (var item in listData)
            {
                try
                {
                    res = nondpat.Update(item);
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res });
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete DPAT tanpa context
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {

            foreach (var id in arrInput)
            {
                nondpat.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = null });
        }

        /// <summary>
        /// GetDocumentNumber
        /// </summary>
        /// <returns></returns>
        [Route("GetNonDpatNumber", Name = "GetNonDpatNumber")]
        [HttpGet]
        public IActionResult GetNonDpatNumber()
        {
            try
            {
                var docNo = nondpat.GetDocumentNumber();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = docNo });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
    }
}
