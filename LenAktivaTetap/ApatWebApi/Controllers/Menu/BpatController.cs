using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Cors;
using ApatDataAccess.Repository;
using ApatLog;

/// <summary>
/// Namescape Controllers
/// </summary>
namespace ApatWebApi.Controllers
{
    /// <summary>
    /// Controller BPAT
    /// </summary>
    [EnableCors("ApatPolicy")]
    [Produces("application/json")]
    [Route("api/Bpat")]
    public class BpatController : Controller
    {
        /// <summary>
        /// Instance Response Object
        /// </summary>
        ResponseObject r = new ResponseObject();
        /// <summary>
        /// Instance Repository BPAT
        /// </summary>
        BpatRepository bpat = new BpatRepository();
        /// <summary>
        /// Exception Trap
        /// </summary>
        Exception error = new Exception();
        Bpat response = new Bpat();

        // GET: api/Bpat
        /// <summary>
        /// Fungsi untuk mengambil seluruh data BPAT
        /// </summary>
        /// <param name="offset">Index List data BPAT</param>
        /// <returns>JSON data BPAT, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetBpat(int offset)
        {
            try
            {
                IList<Bpat> res = bpat.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
            //var res = bpat.GetData();
            //return Ok(res.Result("Success", 200));

        }

        // GET: api/Bpat/5
        /// <summary>
        /// Mengambil data BPAT berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/bpat/GetBpatById?bpatId=1
        /// </summary>
        /// <param name="id">ID BPAT dalam bentuk angka</param>
        /// <returns>Item BPAT</returns>
        [HttpGet("GetBpatById", Name = "GetBpatById")]
        public IActionResult GetBpatById(int bpatId)
        {
            try
            {
                IList<Bpat> res = bpat.GetById(bpatId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/Bpat/5
        /// <summary>
        /// Mengambil data BPAT berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/bpat/GetBpatByNo?bpatNo=1
        /// </summary>
        /// <param name="id">ID BPAT dalam bentuk angka</param>
        /// <returns>Item BPAT</returns>
        [HttpGet("GetBpatByNo", Name = "GetBpatByNo")]
        public IActionResult GetByNo(string bpatNo)
        {
            try
            {
                var res = bpat.GetByNo(bpatNo);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }

        }

        // POST: api/Bpat
        /// <summary>
        /// Menyimpan data BPAT
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Bpat> listData = arrInput.ToObject<List<Bpat>>();
            var allOk = false;

            foreach (var item in listData)
            {
                try
                {
                    item.CreatedDate = DateTime.Now;
                    bpat.Insert(item);
                    response = item;
                    allOk = true;
                    
                }
                catch (Exception e)
                {
                    error = e;
                    e.SaveToFile();
                }

            }

            if (allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = response.BpatId });
            }
            else
            {
                return BadRequest(error);
            }

        }

        // PUT: api/Bpat/5
        /// <summary>
        /// Update Bpat
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<Bpat> listData = arrInput.ToObject<List<Bpat>>();
            var res = new Bpat();

            foreach (var item in listData)
            {
                try
                {
                    res = bpat.Update(item);
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res });
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// API untuk hapus BPAT
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {

            foreach (var id in arrInput)
            {
                bpat.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = arrInput });
        }

        /// <summary>
        /// API untuk melakaukan generate Nomor Dokumen
        /// </summary>
        /// <returns></returns>
        [Route("GetBpatNumber", Name = "GetBpatNumber")]
        [HttpGet]
        public IActionResult GetBpatNumber()
        {
            try
            {
                var docNo = bpat.GetDocumentNumber();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = docNo });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
    }
}
