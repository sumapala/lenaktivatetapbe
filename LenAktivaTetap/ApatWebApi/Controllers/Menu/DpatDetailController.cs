using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/DpatDetail")]
    public class DpatDetailController : Controller
    {
        ApatContext db = ApatContext.Connect();
        List<ResponseObject> r = new List<ResponseObject>();

        /// <summary>
        /// GetDpatDetail
        /// </summary>
        /// <returns></returns>
        // GET: api/DpatDetail
        [HttpGet]
        public IActionResult GetDpatDetail()
        {
            var res = db.DpatDetail;
            return Ok(res);
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/DpatDetail/5
        [HttpGet("{id}", Name = "GetDpatDetail")]
        public IActionResult Get(int id)
        {
            var res = db.Dpat.Where(f => f.DpatId == id);
            return Ok(res);
        }
        

        /// <summary>
        /// POST
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // POST: api/DpatDetail
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]JArray arrInput)
        {
            List<DpatDetail> listData = arrInput.ToObject<List<DpatDetail>>();
            var allOk = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in listData)
                {
                    if (db.DpatDetail.Any(e => e.DpatDetailId == item.DpatDetailId))
                    {
                        allOk = 1;
                    }
                    else
                    {
                        db.DpatDetail.Add(item);
                        await db.SaveChangesAsync();

                    }
                }

                if (allOk == 0)
                {
                    transaction.Commit();
                    return Ok(db.DpatDetail);
                }
                else
                {
                    transaction.Rollback();
                    return BadRequest();
                }
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // PUT: api/DpatDetail/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]JArray arrInput)
        {
            List<DpatDetail> listData = arrInput.ToObject<List<DpatDetail>>();
            var allOk = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in listData)
                {
                    if (!IsExists(item.DpatDetailId))
                    {
                        allOk = 1;
                    }
                    else
                    {
                        try
                        {
                            db.Entry(item).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                        catch (DbUpdateException e)
                        {
                            return Ok(e.Message);
                        }

                    }
                }

                if (allOk == 0)
                {
                    transaction.Commit();
                    return Ok(db.DpatDetail);
                }
                else
                {
                    transaction.Rollback();
                    return BadRequest();
                }


            }
        }
        
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
            int allOk = 0;
            DpatTransaction dpatTrx = new DpatTransaction();

            bool res;

            foreach (var id in arrInput)
            {
                try
                {
                    res = dpatTrx.DeleteDetail(id);

                }
                catch (Exception)
                {
                    allOk = 0;
                }
            }

            if (allOk == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Cek Record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IsExists(int id)
        {
            return db.DpatDetail.Count(e => e.DpatDetailId == id) > 0;
        }
    }
}
