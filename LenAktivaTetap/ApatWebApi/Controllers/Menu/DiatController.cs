using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Diat")]
    public class DiatController : Controller
    {
        DiatRepository diat = new DiatRepository();
        List<ResponseObject> r = new List<ResponseObject>();

        // GET: api/Diat
        /// <summary>
        /// Get All Diat Master Data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetDiat(string offset)
        {
            try
            {
                var res = diat.GetAll();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res , Total = res.Count()});
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/Diat/5
        /// <summary>
        /// Get Diat Detail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDiatById", Name = "GetDiatById")]
        public IActionResult GetDiat(int diatId)
        {
            try
            {
                var res = diat.GetById(diatId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // GET: api/Dpat/5
        /// <summary>
        /// Get DIAT Detail by No
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[HttpGet("GetDiatByNo", Name = "GetDiatByNo")]
        //public IActionResult GetByNo(string diatNo)
        //{
        //    try
        //    {
        //        var res = diat.GetByNo(diatNo);
        //        return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res, Total = res.Count() });
        //    }
        //    catch (HttpRequestException e)
        //    {
        //        return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
        //    }
        //}

        // POST: api/Diat
        /// <summary>
        /// Insert Diat with JArray param from Angular2
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Diat> listData = arrInput.ToObject<List<Diat>>();
            bool allOk = false;

            Diat response = new Diat();
            Exception ex = new Exception();

            foreach (var item in listData)
            {
                try
                {
                    item.CreatedDate = DateTime.Now;
                    diat.Insert(item);
                    response = item;
                    allOk = true;

                }
                catch (Exception e)
                {
                    ex = e;
                }

            }

            if(allOk)
            {
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = response.DiatId });
            }
            else
            {
                return NotFound(new ResponseObject { Status = 500, Message = "Failed", Data = ex });
            }
        }

        // PUT: api/Diat/5
        /// <summary>
        /// Update Diat
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<Diat> listData = arrInput.ToObject<List<Diat>>();
            Diat res = new Diat();

            foreach (var item in listData)
            {
                try
                {
                    res = diat.Update(item);
                }
                catch (Exception e)
                {
                    return NotFound(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }

            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res });
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete Diat tanpa context
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
            foreach (var id in arrInput)
            {
                diat.Delete(id);
            }

            return Ok(new ResponseObject { Status = 200, Message = "Success", Data = null });
        }

        /// <summary>
        /// GetDocumentNumber
        /// </summary>
        /// <returns></returns>
        [Route("GetDiatNumber", Name = "GetDiatNumber")]
        [HttpGet]
        public IActionResult GetDiatNumber()
        {
            try
            {
                var docNo = diat.GetDocumentNumber();
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = docNo });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }
    }
}
