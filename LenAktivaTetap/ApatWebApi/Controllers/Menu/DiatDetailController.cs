using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Models;
using ApatDataAccess.Transactions;
using ApatDataAccess;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using System.Net.Http;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/DiatDetail")]
    public class DiatDetailController : Controller
    {
        ApatContext db = ApatContext.Connect();
        List<ResponseObject> r = new List<ResponseObject>();
        DiatRepository diatDetail = new DiatRepository();

        /// <summary>
        /// GetDiatDetail
        /// </summary>
        /// <returns></returns>
        // GET: api/DiatDetail
        [HttpGet]
        public IActionResult GetDiatDetail()
        {
            var res = db.DiatDetail;
            return Ok(res);
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/DiatDetail/5
        [HttpGet("{id}", Name = "GetDiatDetail")]
        public IActionResult Get(int id)
        {
            try
            {
                var res = diatDetail.GetById(id);
                return Ok(new ResponseObject { Status = 200, Message = "Success", Data = res });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }


        /// <summary>
        /// POST
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // POST: api/DiatDetail
        [HttpPost]
        public IActionResult Post([FromBody]JArray arrInput)
        {
            List<Diat> listData = arrInput.ToObject<List<Diat>>();

            foreach (var item in listData)
            {
                //try
                //{
                    diatDetail.Insert(item);

                //}
                //catch (Exception e)
                //{
                //    return NotFound(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                //}

            }

            return Ok(new ResponseObject { Status = 200, Message = "Insert Success", Data = null });
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // PUT: api/DiatDetail/5
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<DiatDetail> listData = arrInput.ToObject<List<DiatDetail>>();

            foreach (var item in listData)
            {
                //try
                //{
                //diatDetail.UpdateDetail(item);

                //}
                //catch (Exception e)
                //{
                //    return NotFound(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                //}

            }

            return Ok(new ResponseObject { Status = 200, Message = "Update Success", Data = null });
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public IActionResult Delete([FromBody]int[] arrInput)
        {
            int allOk = 0;
            DiatTransaction diatTrx = new DiatTransaction();

            bool res;

            foreach (var id in arrInput)
            {
                try
                {
                    res = diatTrx.DeleteDetail(id);

                }
                catch (Exception)
                {
                    allOk = 0;
                }
            }

            if (allOk == 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Cek Record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IsExists(int id)
        {
            return db.DiatDetail.Count(e => e.DiiId == id) > 0;
        }
    }
}
