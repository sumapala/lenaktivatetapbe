﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authentication;

namespace ApatWebApi.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [Authorize]
        public async Task<IActionResult> Contact()
        {
            var accessToken = await HttpContext.Authentication.GetTokenAsync("access_token");
            return Ok(accessToken);
            //try
            //{
            //    var accessToken = await HttpContext.Authentication.GetTokenAsync("access_token");

            //    //var client = new HttpClient();

            //    // ignore SSL certificate error : https://blog.roushtech.net/2016/12/20/ignoring-ssl-certificate-errors-net-core-httpclient/
            //    using (var httpClientHandler = new HttpClientHandler())
            //    {
            //        httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };

            //        using (HttpClient client = new HttpClient(httpClientHandler))
            //        {
            //            client.SetBearerToken(accessToken);

            //            //HttpResponseMessage apiResponse = await client.GetAsync("http://localhost:5001/Karyawan/GetManagerGeneralAffair");
            //            HttpResponseMessage apiResponse = await client.GetAsync("https://devtest.intern.len.co.id/silenAPICore/Karyawan/GetManagerGeneralAffair");

            //            if (apiResponse != null)
            //            {
            //                string apiReturnValue = await apiResponse.Content.ReadAsStringAsync();

            //                //KaryawanInfo karyawanInfo = JsonConvert.DeserializeObject<KaryawanInfo>(apiReturnValue);

            //                //if (karyawanInfo != null)
            //                //    ViewData["Message"] = karyawanInfo.NIK + " " + karyawanInfo.Nama;  //"Your contact page."; //

            //                return Ok(apiReturnValue);
            //            }
            //            else
            //                ViewData["Message"] = "Web API tidak dapat diakses";
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    ViewData["Message"] = e.Message;
            //}

            //return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
