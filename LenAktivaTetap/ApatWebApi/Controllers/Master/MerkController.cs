using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Repository;
using ApatDataAccess;

namespace ApatWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Merk")]
    public class MerkController : Controller
    {
        MerkRepository merk = new MerkRepository();

        // GET: api/Merk
        [HttpGet]
        public IActionResult GetMerk(int offset)
        {
            return Ok(merk.GetAll().Result("Success", 200, offset));
        }

        // GET: api/Merk/5
        [HttpGet("{id}", Name = "GetMerkById")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Merk
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Merk/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


    }
}
