﻿using Microsoft.AspNetCore.Mvc;
using ApatDataAccess;
using ApatDataAccess.Repository.Master;
using System.Collections.Generic;
using ApatDataAccess.Models.Master;
using System.Net.Http;
using ApatLog;

/// <summary>
/// Controller untuk master Tipe Barang
/// Digunakan untuk meng-handle request data Tipe Barang
/// </summary>
namespace ApatWebApi.Controllers.Master
{
    /// <summary>
    /// Controller Tipe Barang menggunakan route api/mastertipe
    /// </summary>
    [Produces("application/json")]
    [Route("api/mastertipe")]
    public class TipeController : Controller
    {
        /// <summary>
        /// Instance untuk repository Tipe Barang
        /// </summary>
        TipeRepository Tipe = new TipeRepository();

        /// <summary>
        /// Mengambil data Tipe Barang
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/mastertipe
        /// </summary>
        /// <param name="offset">Index List data kemlompok</param>
        /// <returns>JSON data Tipe, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetTipe(int offset)
        {
            try
            {
                IList<MasterTipe> res = Tipe.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Tipe Barang berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/mastertipe/GetTipeById?TipeId=1
        /// </summary>
        /// <param name="TipeId">ID Tipe Barang dalam bentuk angka</param>
        /// <returns>Item Tipe Barang</returns>
        [HttpGet("GetTipeById", Name = "GetTipeById")]
        public IActionResult GetTipeById(int TipeId)
        {
            try
            {
                IList<MasterTipe> res = Tipe.GetById(TipeId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Tipe Barang berdasarkan Jenis ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/mastertipe/getTipeByJenisId?JenisId=5
        /// </summary>
        /// <param name="jenisId">ID Jenis Barang dalam bentuk angka</param>
        /// <returns>Item Tipe Barang</returns>
        [HttpGet("getTipeByJenisId", Name = "getTipeByJenisId")]
        public IActionResult getTipeByJenisId(int jenisId)
        {
            try
            {
                IList<MasterTipe> res = Tipe.GetByJenisId(jenisId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Memasukan data Tipe Barang ke database
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/mastertipe
        /// </summary>
        /// <param name="value">Data Tipe yang akan dimasukan</param>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// Mengubah data Tipe Barang berdasarkan ID tertentu
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/mastertipe/5
        /// </summary>
        /// <param name="id">ID Tipe Barang</param>
        /// <param name="value">Data Tipe yang akan diubah</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        /// <summary>
        /// Menghapus data Tipe Barang berdasarkan ID tertentu
        /// Menggunakan method DELETE
        /// Contoh Penggunaan : api/mastertipe/5
        /// </summary>
        /// <param name="id">ID Tipe Barang</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
