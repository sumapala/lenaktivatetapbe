using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess.Repository;
using ApatDataAccess;
using System.Net.Http;

/// <summary>
/// Namespace Controller
/// </summary>
namespace ApatWebApi.Controllers
{
    /// <summary>
    /// API Class User Controller
    /// </summary>
    [Produces("application/json")]
    [Route("api/UserRole")]
    public class UserRoleController : Controller
    {
        /// <summary>
        /// Inisiasi repositori User Role
        /// </summary>
        UserRoleRepository userrole = new UserRoleRepository();

        // GET: api/UserRole
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// API Baca data User Role berdasarkan paramater UserId
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>HTTP Response JSON</returns>
        // GET: api/UserRole/5
        [HttpGet("GetUserRoleById", Name = "GetUserRoleById")]
        public IActionResult GetUserRoleById(string UserId)
        {

            int intUserId = Int32.Parse(Base64Decode(UserId));

            try
            {
                IList<object> res = userrole.GetById(intUserId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        // POST: api/UserRole
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/UserRole/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        /// <summary>
        /// Base64 Decode Generik
        /// </summary>
        /// <param name="base64EncodedData"></param>
        /// <returns></returns>
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
