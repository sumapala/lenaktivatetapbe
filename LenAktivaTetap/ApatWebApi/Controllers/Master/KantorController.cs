using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApatDataAccess;
using ApatDataAccess.Repository;

namespace ApatWebApi.Controllers.Master
{
    [Produces("application/json")]
    [Route("api/Kantor")]
    public class KantorController : Controller
    {
        KantorRepository kantor = new KantorRepository();
        // GET: api/Kantor
        [HttpGet]
        public IActionResult GetKantor(int offset)
        {
            return Ok(kantor.GetAll().Result("Success", 200, offset));
        }

        // GET: api/Kantor/5
        [HttpGet("{id}", Name = "GetKantorById")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Kantor
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Kantor/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
