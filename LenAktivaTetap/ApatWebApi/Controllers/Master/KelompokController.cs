﻿using Microsoft.AspNetCore.Mvc;
using ApatDataAccess;
using ApatDataAccess.Repository.Master;
using System.Net.Http;
using System.Collections.Generic;
using ApatDataAccess.Models.Master;
using ApatLog;

/// <summary>
/// Controller untuk master Kelompok Barang
/// Digunakan untuk meng-handle request data Kelompok Barang
/// </summary>
namespace ApatWebApi.Controllers.Master
{
    /// <summary>
    /// Controller Kelompok Barang menggunakan route api/masterkelompok
    /// </summary>
    [Produces("application/json")]
    [Route("api/masterkelompok")]
    public class KelompokController : Controller
    {
        /// <summary>
        /// Instance untuk repository Kelompok Barang
        /// </summary>
        KelompokRepository Kelompok = new KelompokRepository();

        /// <summary>
        /// Mengambil data Kelompok Barang
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/masterkelompok
        /// </summary>
        /// <param name="offset">Index List data kemlompok</param>
        /// <returns>JSON data Kelompok, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetKelompok(int offset)
        {
            try
            {
                IList<MasterKelompok> res = Kelompok.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Kelompok Barang berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/masterkelompok/GetKelompokById?kelompokId=1
        /// </summary>
        /// <param name="id">ID Kelompok Barang dalam bentuk angka</param>
        /// <returns>Item Kelompok Barang</returns>
        [HttpGet("GetKelompokById", Name = "GetKelompokById")]
        public IActionResult GetKelompokById(int kelompokId)
        {
            try
            {
                IList<MasterKelompok> res = Kelompok.GetById(kelompokId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Memasukan data Kelompok Barang ke database
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/masterkelompok
        /// </summary>
        /// <param name="value">Data Kelompok yang akan dimasukan</param>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// Mengubah data Kelompok Barang berdasarkan ID tertentu
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/masterkelompok/5
        /// </summary>
        /// <param name="id">ID Kelompok Barang</param>
        /// <param name="value">Data Kelompok yang akan diubah</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        /// <summary>
        /// Menghapus data Kelompok Barang berdasarkan ID tertentu
        /// Menggunakan method DELETE
        /// Contoh Penggunaan : api/masterkelompok/5
        /// </summary>
        /// <param name="id">ID Kelompok Barang</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
