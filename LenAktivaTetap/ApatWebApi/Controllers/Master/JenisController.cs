﻿using Microsoft.AspNetCore.Mvc;
using ApatDataAccess;
using ApatDataAccess.Repository.Master;
using System.Collections.Generic;
using ApatDataAccess.Models.Master;
using System.Net.Http;
using ApatLog;

/// <summary>
/// Controller untuk master Jenis Barang
/// Digunakan untuk meng-handle request data Jenis Barang
/// </summary>
namespace ApatWebApi.Controllers.Master
{
    /// <summary>
    /// Controller Jenis Barang menggunakan route api/masterjenis
    /// </summary>
    [Produces("application/json")]
    [Route("api/masterjenis")]
    public class JenisController : Controller
    {
        /// <summary>
        /// Instance untuk repository Jenis Barang
        /// </summary>
        JenisRepository Jenis = new JenisRepository();

        /// <summary>
        /// Mengambil data Jenis Barang
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/masterjenis
        /// </summary>
        /// <param name="offset">Index List data kemlompok</param>
        /// <returns>JSON data Jenis, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetJenis(int offset)
        {
            try
            {
                IList<MasterJenis> res = Jenis.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Jenis Barang berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/masterjenis/GetJenisById?JenisId=1
        /// </summary>
        /// <param name="JenisId">ID Jenis Barang dalam bentuk angka</param>
        /// <returns>Item Jenis Barang</returns>
        [HttpGet("GetJenisById", Name = "GetJenisById")]
        public IActionResult GetJenisById(int JenisId)
        {
            try
            {
                IList<MasterJenis> res = Jenis.GetById(JenisId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Jenis Barang berdasarkan Sub Kelompok ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/masterjenis/getJenisBySub KelompokId?Sub KelompokId=5
        /// </summary>
        /// <param name="subKelompokId">ID Sub Kelompok Barang dalam bentuk angka</param>
        /// <returns>Item Jenis Barang</returns>
        [HttpGet("getJenisBySubKelompokId", Name = "getJenisBySubKelompokId")]
        public IActionResult GetJenisBySubKelompokId(int subKelompokId)
        {
            try
            {
                IList<MasterJenis> res = Jenis.GetBySubKelompokId(subKelompokId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Memasukan data Jenis Barang ke database
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/masterjenis
        /// </summary>
        /// <param name="value">Data Jenis yang akan dimasukan</param>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// Mengubah data Jenis Barang berdasarkan ID tertentu
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/masterjenis/5
        /// </summary>
        /// <param name="id">ID Jenis Barang</param>
        /// <param name="value">Data Jenis yang akan diubah</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        /// <summary>
        /// Menghapus data Jenis Barang berdasarkan ID tertentu
        /// Menggunakan method DELETE
        /// Contoh Penggunaan : api/masterjenis/5
        /// </summary>
        /// <param name="id">ID Jenis Barang</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
