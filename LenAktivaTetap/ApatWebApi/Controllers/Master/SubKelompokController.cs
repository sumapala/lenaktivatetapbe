﻿using Microsoft.AspNetCore.Mvc;
using ApatDataAccess;
using ApatDataAccess.Repository.Master;
using System.Collections.Generic;
using ApatDataAccess.Models.Master;
using System.Net.Http;
using ApatLog;

/// <summary>
/// Controller untuk master Sub Kelompok Barang
/// Digunakan untuk meng-handle request data Sub Kelompok Barang
/// </summary>
namespace ApatWebApi.Controllers.Master
{
    /// <summary>
    /// Controller Sub Kelompok Barang menggunakan route api/mastersubkelompok
    /// </summary>
    [Produces("application/json")]
    [Route("api/mastersubkelompok")]
    public class SubKelompokController : Controller
    {
        /// <summary>
        /// Instance untuk repository Sub Kelompok Barang
        /// </summary>
        SubKelompokRepository SubKelompok = new SubKelompokRepository();

        /// <summary>
        /// Mengambil data Sub Kelompok Barang
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/mastersubkelompok
        /// </summary>
        /// <param name="offset">Index List data kemlompok</param>
        /// <returns>JSON data Sub Kelompok, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetSubKelompok(int offset)
        {
            try
            {
                IList<MasterSubKelompok> res = SubKelompok.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Sub Kelompok Barang berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/mastersubkelompok/GetSubKelompokById?subKelompokId=1
        /// </summary>
        /// <param name="subKelompokId">ID Sub Kelompok Barang dalam bentuk angka</param>
        /// <returns>Item Sub Kelompok Barang</returns>
        [HttpGet("GetSubKelompokById", Name = "GetSubKelompokById")]
        public IActionResult GetSubKelompokById(int subKelompokId)
        {
            try
            {
                IList<MasterSubKelompok> res = SubKelompok.GetById(subKelompokId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data Sub Kelompok Barang berdasarkan Kelompok ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/mastersubkelompok/getSubKelompokByKelompokId?kelompokId=5
        /// </summary>
        /// <param name="kelompokId">ID Kelompok Barang dalam bentuk angka</param>
        /// <returns>Item Sub Kelompok Barang</returns>
        [HttpGet("getSubKelompokByKelompokId", Name = "getSubKelompokByKelompokId")]
        public IActionResult getSubKelompokByKelompokId(int kelompokId)
        {
            try
            {
                IList<MasterSubKelompok> res = SubKelompok.GetByKelompokId(kelompokId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Memasukan data SubKelompok Barang ke database
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/mastersubkelompok
        /// </summary>
        /// <param name="value">Data Sub Kelompok yang akan dimasukan</param>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// Mengubah data Sub Kelompok Barang berdasarkan ID tertentu
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/mastersubkelompok/5
        /// </summary>
        /// <param name="id">ID Sub Kelompok Barang</param>
        /// <param name="value">Data Sub Kelompok yang akan diubah</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        /// <summary>
        /// Menghapus data Sub Kelompok Barang berdasarkan ID tertentu
        /// Menggunakan method DELETE
        /// Contoh Penggunaan : api/mastersubkelompok/5
        /// </summary>
        /// <param name="id">ID Sub Kelompok Barang</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
