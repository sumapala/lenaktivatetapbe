﻿using Microsoft.AspNetCore.Mvc;
using ApatDataAccess;
using ApatDataAccess.Repository.List;
using System.Collections.Generic;
using ApatDataAccess.Models.List;
using System.Net.Http;
using ApatLog;
using Newtonsoft.Json.Linq;
using System;

/// <summary>
/// Controller untuk List Approval Barang
/// Digunakan untuk meng-handle request data List Approval Barang
/// </summary>
namespace ApatWebApi.Controllers.List
{
    /// <summary>
    /// Controller List Approval Barang menggunakan route api/ListApproval
    /// </summary>
    [Produces("application/json")]
    [Route("api/ListApproval")]
    public class ApprovalController : Controller
    {
        /// <summary>
        /// Instance untuk repository List Approval Barang
        /// </summary>
        ApprovalRepository Approval = new ApprovalRepository();

        /// <summary>
        /// Mengambil data List Approval Barang
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/ListApproval
        /// </summary>
        /// <param name="offset">Index List data</param>
        /// <returns>JSON data Approval, dikirim ke client</returns>
        [HttpGet]
        public IActionResult GetApproval(int offset)
        {
            try
            {
                IList<ListApproval> res = Approval.GetAll();
                return Ok(res.Result("Success", 200, offset));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data List Approval Barang berdasarkan ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/ListApproval/GetApprovalById?ApprovalId=1
        /// </summary>
        /// <param name="ApprovalId">ID List Approval Barang dalam bentuk angka</param>
        /// <returns>Item List Approval Barang</returns>
        [HttpGet("GetApprovalById", Name = "GetApprovalById")]
        public IActionResult GetApprovalById(int ApprovalId)
        {
            try
            {
                IList<ListApproval> res = Approval.GetById(ApprovalId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data List Approval Barang berdasarkan User ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/ListApproval/getByUserId?UserId=5
        /// </summary>
        /// <param name="subKelompokId">ID User dalam bentuk angka</param>
        /// <returns>Item List Approval Barang</returns>
        [HttpGet("getByUserId", Name = "getByUserId")]
        public IActionResult GetByUserId(int userId)
        {
            try
            {
                IList<ListApproval> res = Approval.GetByUserId(userId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Mengambil data dalam bentuk List AT pada database DII
        /// Menggunakan Approval ID tertentu
        /// Menggunakan method GET
        /// Contoh Penggunaan : api/ListApproval/GetATByApprovalId?listApprovalId=5
        /// </summary>
        /// <param name="listApprovalId">ID Approval dalam bentuk angka</param>
        /// <returns>Item List DII Barang</returns>
        [HttpGet("getATByApprovalId", Name = "getATByApprovalId")]
        public IActionResult GetATByApprovalId(int listApprovalId)
        {
            try
            {
                IList<Object> res = Approval.GetATByApprovalId(listApprovalId);
                return Ok(res.Result("Success", 200));
            }
            catch (HttpRequestException e)
            {
                e.SaveToFile();
                return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
            }
        }

        /// <summary>
        /// Memasukan data List Approval Barang ke database
        /// Menggunakan method POST
        /// Contoh Penggunaan : api/ListApproval
        /// </summary>
        /// <param name="value">Data Approval yang akan dimasukan</param>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// Update data List Approval Barang pada database
        /// PUT: api/Babh
        /// </summary>
        /// <param name="arrInput">Input dari body</param>
        /// <returns>Format informasi berdasarkan IActionResult</returns>
        [HttpPut]
        public IActionResult Put([FromBody]JArray arrInput)
        {
            List<ListApproval> listData = arrInput.ToObject<List<ListApproval>>();

            foreach (var item in listData)
            {
                try
                {
                    Approval.Update(item);
                }
                catch (Exception e)
                {
                    return BadRequest(new ResponseObject { Status = 500, Message = "Failed", Data = e });
                }
            }

            return Ok(new ResponseObject { Status = 200, Message = "Update Success", Data = null });
        }

        /// <summary>
        /// Menghapus data List Approval Barang berdasarkan ID tertentu
        /// Menggunakan method DELETE
        /// Contoh Penggunaan : api/ListApproval/5
        /// </summary>
        /// <param name="id">ID List Approval Barang</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
